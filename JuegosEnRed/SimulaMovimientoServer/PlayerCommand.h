#pragma once
#include <vector>
class PlayerCommand
{
private:
	int idPlayer;
	int idMove;
	int absolutePosition;
	bool fire;
public:
	PlayerCommand();
	PlayerCommand(int _idPlayer, int _idMove, int _absolutePosition, bool _fire);
	PlayerCommand(const PlayerCommand& _playerCommand);
	int GetIdPlayer();
	int GetIdMove();
	int GetAbsolutePosition();
	bool GetFire();
	~PlayerCommand();
};

