#include "PlayerCommand.h"



PlayerCommand::PlayerCommand()
{
}

PlayerCommand::PlayerCommand(int _idPlayer, int _idMove, int _absolutePosition, bool _fire): idPlayer(_idPlayer), idMove(_idMove),
	absolutePosition(_absolutePosition), fire(_fire)
{
}

PlayerCommand::PlayerCommand(const PlayerCommand & _playerCommand):
	PlayerCommand(_playerCommand.idPlayer, _playerCommand.idMove, _playerCommand.absolutePosition, _playerCommand.fire)
{
}

int PlayerCommand::GetIdPlayer()
{
	return idPlayer;
}

int PlayerCommand::GetIdMove()
{
	return idMove;
}

int PlayerCommand::GetAbsolutePosition()
{
	return absolutePosition;
}

bool PlayerCommand::GetFire()
{
	return fire;
}

PlayerCommand::~PlayerCommand()
{
}
