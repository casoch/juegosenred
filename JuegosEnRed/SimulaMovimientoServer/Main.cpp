#include "Game.h"
#include <SocketTools.h>

int main(int argc, char ** argv) {
	SocketTools::CargarLibreria();
	std::string funcion = argv[1]; //jugador - servidor
	std::string serverAddress = argv[2];

	Game game("Movement", SCREEN_WIDTH, SCREEN_HEIGHT, funcion, serverAddress);

	try {
		game.run();
	}
	catch (std::exception e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
	}
	SocketTools::DescargarLibreria();
	return 0;
}