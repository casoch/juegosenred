#include "SocketUtil.h"



UDPSocketPtr SocketUtil::CreateUDPSocket()
{
	SOCKET s = socket(AF_INET, SOCK_DGRAM, 0);
	if (s != INVALID_SOCKET)
	{
		return UDPSocketPtr(new UDPSocket(s));
	}
	else
	{
		SocketUtil::ReportError(L"SocketUtil::CreateUDPSocket");
		return nullptr;
	}
}

int SocketUtil::CargarLibreria()
{
	WSADATA wsa;
	int result = WSAStartup(MAKEWORD(2, 2), &wsa);
	return result;
}

int SocketUtil::DescargarLibreria()
{
	int result = WSACleanup();
	return result;
}

void SocketUtil::ReportError(PCWSTR error)
{
	std::cout << error << std::endl;
}

int SocketUtil::GetLastError()
{
	return 0;
}
