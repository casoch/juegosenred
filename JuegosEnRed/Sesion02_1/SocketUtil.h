#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include <Windows.h>
#include <memory>
#include "UDPSocket.h"

typedef std::shared_ptr<UDPSocket> UDPSocketPtr;

enum SocketAddressFamily { INET = AF_INET, INET6 = AF_INET6 };


class SocketUtil
{

public:

	static UDPSocketPtr CreateUDPSocket();

	static int CargarLibreria();

	static int DescargarLibreria();

	static void ReportError(PCWSTR error);

	static int GetLastError();
};
