#include "Server.h"
#include "List.h"

std::string Server::CreateMsgRanking()
{
	std::string ranking = "SCORE_";

	List<int> aIndicesMarcados;

	for (size_t j = 0; j < aClients.size(); j++)
	{
		int max = -1;
		int indexMax = -1;
		for (int i = 0; i < aClients.size(); i++)
		{
			int scoreAct = aClients[i].GetScore();
			if (scoreAct > max)
			{
				if (aIndicesMarcados.IndexOf(i) == -1)
				{
					max = scoreAct;
					indexMax = i;
				}
			}
		}
		aIndicesMarcados.push_back(indexMax);
		std::string nick = aClients[indexMax].GetNick();
		int score = aClients[indexMax].GetScore();
		ranking = ranking.append(nick);
		ranking = ranking.append(":");
		ranking = ranking.append(std::to_string(score));
		if (j < aClients.size() - 1)
		{
			ranking = ranking.append("#");
		}
	}
	return ranking;
}

Server::Server(std::string _serverAddress, int _numPlayers)
{
	aWords.LoadDefault();
	SocketAddress sa;
	sa.SetAddress(_serverAddress);
	int err = dispatcher.Bind(sa);
	if (err == -1)
	{
		throw std::exception("Error en Server::Server");
	}
	err = dispatcher.Listen(_numPlayers);
	if (err == -1)
	{
		throw std::exception("Error en listen");
	}
}

void Server::LoadingPlayers(int _numPlayers)
{
	for (int i = 0; i < _numPlayers; i++)
	{
		ClientProxy client;
		SocketAddress from;
		std::cout << "Esperando cliente " << (i + 1) << std::endl;
		std::shared_ptr<TCPSocket> tcpSocket = dispatcher.Accept(from);
		tcpSocket->NonBlocking(true);
		client.SetSocket(tcpSocket);
		aClients.push_back(client);
		std::cout << "Llega cliente " << (i + 1) << std::endl;
	}

	SendMsgAll("BEGIN");
	std::cout << "Empieza el juego" << std::endl;
}

void Server::SendMsgAll(std::string _message)
{
	for (int i = 0; i < aClients.size(); i++)
	{
		int err = aClients[i].Send(_message);
		if (err == -1)
		{
			std::cout << "Error al enviar al cliente " << i << std::endl;
		}
	}
}

void Server::ReceiveAndProcessMsgClients()
{
	for (int i = 0; i < aClients.size(); i++)
	{
		std::string message;
		int err = aClients[i].Receive(message);
		if (err == 0)
		{
			aClients.erase(aClients.begin() + i);
		}
		else if (err > 0)
		{
			bool exit = ProcessMessage(message, i);
			if (exit)
			{
				aClients.erase(aClients.begin() + i);
			}
		}
	}
}

bool Server::ProcessMessage(std::string _message, int _numClient)
{
	int index_ = _message.find_first_of('_');
	std::string cabecera = _message.substr(0, index_);
	std::string contenido = _message.substr(index_ + 1, _message.size() - index_);
	if (cabecera == "NICK")
	{
		aClients[_numClient].SetNick(contenido);
	}
	else if (cabecera == "WRITE")
	{
		//std::cout << "Llega la palabra " << contenido << "de " << idClient<< std::endl;
		bool equals = aWords.Compare(contenido);
		if (!equals)
		{
			aClients[_numClient].Send("KOWORD");
		}
		else
		{
			std::string msgSend = "OKWORD_";
			msgSend = msgSend.append(aClients[_numClient].GetNick());
			aClients[_numClient].AddScore();
			SendMsgAll(msgSend);
			msgSend = CreateMsgRanking();
			SendMsgAll(msgSend);
			bool endWords = aWords.IncIndex();
			if (endWords)
			{
				return true;
			}
		}
	}
	return false;
}

bool Server::SendWord()
{
	std::string word;
	bool seHaEnviado = aWords.WordToSend(word);
	if (!seHaEnviado)
	{
		aWords.WordSent();
		std::string wordToSent = "WORD_";
		wordToSent = wordToSent.append(word);
		//std::cout << "Env�o la palabra " << wordToSent << std::endl;
		SendMsgAll(wordToSent);
	}
	else if (aWords.EndList())
	{
		return true;
	}
	return false;
}

Server::~Server()
{

}