#pragma once
#include <vector>

class ListWords
{
private:
	std::vector<std::string> aWords;
	int indexWord;
	std::vector<bool> aSent;
public:
	ListWords();
	void LoadFromFile(std::string nameFile);
	void LoadDefault();
	bool WordToSend(std::string& word)
	{
		if (indexWord == aWords.size() || aSent[indexWord] == true)
		{
			return true;
		}
		else
		{
			word = aWords[indexWord];
			return false;
		}
	}

	void WordSent()
	{
		aSent[indexWord] = true;
	}

	bool Compare(std::string word)
	{
		return word == aWords[indexWord];
	}

	bool IncIndex()
	{
		indexWord++;
		if (indexWord == aWords.size())
		{
			return true;
		}
		return false;
	}

	bool EndList()
	{
		return indexWord == aWords.size();
	}
	~ListWords();
};

