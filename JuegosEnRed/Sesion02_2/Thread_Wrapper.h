#pragma once

// Thread_Wrapper.h
#include <WinDef.h>
template<class T>
class Thread_Wrapper
{
public:
	T* object;
	DWORD(T::* method)(void);
public:
	static DWORD start(LPVOID thread_wrapper_obj)
	{
		Thread_Wrapper<T>* wrap = (Thread_Wrapper<T>*)thread_wrapper_obj;
		T* object = wrap->object;
		DWORD(T::* method)(void) = wrap->method;
		(object->*method) ();
		return 0;
	}
	Thread_Wrapper(T* object, DWORD(T::* method)(void))
	{
		this->object = object;
		this->method = method;
	}
	~Thread_Wrapper(void) {}
};
