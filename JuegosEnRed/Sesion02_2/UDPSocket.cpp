#include "UDPSocket.h"
#include "SocketUtil.h"


int UDPSocket::Bind(const SocketAddress& inToAddress)
{
	uint32_t size = inToAddress.GetSize();
	int err = bind(mSocket, &(inToAddress.mSockAddr), size);
	if (err != 0)
	{

		SocketUtil::ReportError(L"UDPSocket::Bind");
		return -SocketUtil::GetLastError();
	}
	return NO_ERROR;
}

int UDPSocket::SendTo(const void* inData, int inLen, const SocketAddress & inTo)
{
	int byteSentCount = sendto(mSocket, (const char*)inData, inLen, 0, &inTo.mSockAddr, inTo.GetSize());
	if (byteSentCount >= 0)
	{
		return byteSentCount;

	}
	else
	{
		SocketUtil::ReportError(L"UDPSocket::SendTo");
		return -SocketUtil::GetLastError();
	}
}

int UDPSocket::ReceiveFrom(void * inBuffer, int inLen, SocketAddress & outFrom)
{
	int fromLen = outFrom.GetSize();
	int readByteCount = recvfrom(mSocket, (char*)inBuffer, inLen, 0, &outFrom.mSockAddr, &fromLen);
	//std::cout << inBuffer << std::endl;
	int longBuffer = strlen((char*)inBuffer);
	//std::cout << "Logitud del buffer: " << longBuffer << std::endl;
	//for (int i = 0; i < longBuffer; i++)
	//{
	//	std::cout << "inBuffer[" << i << "]=" << ((char*)inBuffer)[i] << std::endl;
	//}
	if (readByteCount >= 0)
	{
		return readByteCount;
	}
	else
	{
		SocketUtil::ReportError(L"UDPSocket::ReceiveFrom");
		return -SocketUtil::GetLastError();
	}
}

UDPSocket::~UDPSocket()
{
	closesocket(mSocket);
}
