#pragma once
#include "SocketUtil.h"
#include "StringUtils.h"
#include <vector>
#include <iostream>
#include <thread>
#include <exception>
#include <mutex>
#include <list>


#define WRITE_POS_X 1
#define WRITE_POS_Y 23
#define MENSAJES_POS_X 1
#define MENSAJES_POS_Y 1
#define MAX_MENSAJES 10

PCWSTR miIP;
PCWSTR remotaIP;
int miPuertoRead;
int miPuertoWrite;
int remotoPuerto;

std::mutex miMutex;

std::string aMensajes[MAX_MENSAJES];
int numMensajes = 0;

void gotoxy(int x, int y)
{
	COORD c = { x,y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
}

void Encolar(char mensaje[])
{
	std::string str = mensaje;
	miMutex.lock();
	if (numMensajes == MAX_MENSAJES)
	{
		for (int i = 1; i < MAX_MENSAJES; i++)
		{
			aMensajes[i - 1] = aMensajes[i];
		}
		aMensajes[MAX_MENSAJES - 1] = str;
	}
	else
	{
		aMensajes[numMensajes] = str;
		numMensajes++;
	}
	miMutex.unlock();
}

void MostrarMensajes()
{
	system("cls");
	miMutex.lock();
	gotoxy(MENSAJES_POS_X, MENSAJES_POS_Y);
	for (int i = 0; i < numMensajes; i++)
	{
		std::cout << ">>" << aMensajes[i] << std::endl;
	}
	gotoxy(WRITE_POS_X, WRITE_POS_Y);
	miMutex.unlock();
}

void EscribirYEnviar()
{
	UDPSocketPtr udpSocketPtr = SocketUtil::CreateUDPSocket();
	SocketAddress address(L"127.0.0.1", miPuertoWrite);
	int err = udpSocketPtr->Bind(address);
	if (err == NO_ERROR)
	{
		SocketAddress inTo(L"127.0.0.1", remotoPuerto);
		bool exit = false;
		do {
			char datos[1300];

			gotoxy(WRITE_POS_X, WRITE_POS_Y);
			std::cout << "> ";
			int longitud = StringUtils::PideDatosTeclado(datos);
			//std::cout << "Longitud: " << longitud << std::endl;
			//std::cout << "Estoy enviado " << datos << std::endl;
			//std::cout << "Enviando por " << remotoPuerto << std::endl;
			int sendBytes = udpSocketPtr->SendTo(datos, longitud, inTo);
			if (sendBytes > 0)
			{
				//std::cout << "Se han enviado " << sendBytes << std::endl;
				char datosEncolar[1300];
				strcpy_s(datosEncolar, datos);
				Encolar(datosEncolar);
				
				int cmp = strcmp(datos, "exit");
				if (cmp == 0)
				{
					exit = true;
				}
				MostrarMensajes();
			}
			else
			{
				std::cout << "Error al enviar " << sendBytes << std::endl;
			}
			
		} while (!exit);
	}
}

void EscucharYEscribir()
{
	
	SocketAddress MyAddress(L"127.0.0.1", miPuertoRead);
	UDPSocketPtr udpSocket = SocketUtil::CreateUDPSocket();
	int err = udpSocket->Bind(MyAddress);
	if (err == NO_ERROR)
	{
		//std::cout << "Socket escuchando por " << miPuerto << std::endl;
		char buffer[1300];
		SocketAddress RemoteAddress(MyAddress);
		bool exit = false;
		while (!exit)
		{
			//std::cout << "Esperando" << std::endl;
			int longDatos = udpSocket->ReceiveFrom(buffer, 1300, RemoteAddress);

			if (longDatos > 0)
			{
				buffer[longDatos] = '\0';
				Encolar(buffer);
				//std::cout << buffer << std::endl;
				int cmp = strcmp("exit", buffer);
				if (cmp == 0)
				{
					exit = true;
				}
				MostrarMensajes();
			}
		}
	}
}




void LanzarLectura()
{
	
	//std::cout << "Launching a thread\n";
	std::thread t(EscucharYEscribir);
	
	//std::cout << "detaching the thread\n";
	// do not wait for the thread to finish
	t.detach();
}

void cpp11()
{
	try
	{
		std::cout << "C++11 MULTITHREADING\n";
		for (int i = 0; i < 10000; i++)
			std::cout << i << " ";
	}
	catch (std::exception& e)
	{
		std::cout << "Exception en cpp11: " << e.what() << std::endl;
	}
}



void lectura(std::string mensaje)
{
	for (int i = 0; i < 100; i++)
	{
		std::cout << mensaje << "...." << std::endl;
	}
}

void escritura(std::string mensaje)
{
	for (int i = 0; i < 100; i++)
	{
		std::cout << mensaje << "...." << std::endl;
	}
}

int main(int argc, char** argv)
{
	SocketUtil::CargarLibreria();
	char miIP[30];
	strcpy_s(miIP, argv[1]);
	miPuertoRead = atoi(argv[2]);
	miPuertoWrite = atoi(argv[3]);
	char remotaIP[30];
	strcpy_s(remotaIP, argv[4]);
	remotoPuerto = atoi(argv[5]);
	system("cls");
	
	LanzarLectura();
	EscribirYEnviar();
	
	
	/*std::thread tLeer(lectura, "LECTURA");
	tLeer.detach();
	std::thread tEscritura(escritura, "ESCRITURA");
	tEscritura.detach();
	std::cout << " ***END OF A PROGRAM***\n";*/
	system("pause");
	

	SocketUtil::DescargarLibreria();	
}


