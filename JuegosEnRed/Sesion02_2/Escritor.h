#pragma once
#include <memory>
#include "SocketAddress.h"
#include "UDPSocket.h"
#include "SocketUtil.h"
#include "Buffer.h"

class Escritor
{
private:
	UDPSocketPtr udpSocketPtr;
	int16_t remotoPuerto;
public:
	Escritor(int16_t miPuertoWrite, int16_t remotoPuerto)
	{
		udpSocketPtr = SocketUtil::CreateUDPSocket();
		SocketAddress address(L"127.0.0.1", miPuertoWrite);
		int err = udpSocketPtr->Bind(address);
		this->remotoPuerto = remotoPuerto;
	}

	void Escribir(Buffer& b)
	{
		SocketAddress inTo(L"127.0.0.1", remotoPuerto);
		bool exit = false;
		do {
			char datos[1300];
			std::cout << "> ";
			int longitud = StringUtils::PideDatosTeclado(datos);
			//std::cout << "Longitud: " << longitud << std::endl;
			//std::cout << "Estoy enviado " << datos << std::endl;
			//std::cout << "Enviando por " << remotoPuerto << std::endl;
			int sendBytes = udpSocketPtr->SendTo(datos, longitud, inTo);
			if (sendBytes > 0)
			{
				//std::cout << "Se han enviado " << sendBytes << std::endl;
				char datosEncolar[1300];
				strcpy_s(datosEncolar, datos);
				Encolar(datosEncolar);

				int cmp = strcmp(datos, "exit");
				if (cmp == 0)
				{
					exit = true;
				}
				MostrarMensajes();
			}
			else
			{
				std::cout << "Error al enviar " << sendBytes << std::endl;
			}

		} while (!exit);
	}
};

void EscribirYEnviar()
{
	UDPSocketPtr udpSocketPtr = SocketUtil::CreateUDPSocket();
	SocketAddress address(L"127.0.0.1", miPuertoWrite);
	int err = udpSocketPtr->Bind(address);
	if (err == NO_ERROR)
	{
		SocketAddress inTo(L"127.0.0.1", remotoPuerto);
		bool exit = false;
		do {
			char datos[1300];

			gotoxy(WRITE_POS_X, WRITE_POS_Y);
			std::cout << "> ";
			int longitud = StringUtils::PideDatosTeclado(datos);
			//std::cout << "Longitud: " << longitud << std::endl;
			//std::cout << "Estoy enviado " << datos << std::endl;
			//std::cout << "Enviando por " << remotoPuerto << std::endl;
			int sendBytes = udpSocketPtr->SendTo(datos, longitud, inTo);
			if (sendBytes > 0)
			{
				//std::cout << "Se han enviado " << sendBytes << std::endl;
				char datosEncolar[1300];
				strcpy_s(datosEncolar, datos);
				Encolar(datosEncolar);

				int cmp = strcmp(datos, "exit");
				if (cmp == 0)
				{
					exit = true;
				}
				MostrarMensajes();
			}
			else
			{
				std::cout << "Error al enviar " << sendBytes << std::endl;
			}

		} while (!exit);
	}
}