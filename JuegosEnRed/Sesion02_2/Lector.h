#pragma once
#include <memory>
#include "SocketAddress.h"
#include "UDPSocket.h"
#include "SocketUtil.h"
#include "Buffer.h"
class Lector
{
private:
	UDPSocketPtr udpSocket;

public:
	Lector(uint16_t miPuertoRead)
	{
		SocketAddress MyAddress(L"127.0.0.1", miPuertoRead);
		UDPSocketPtr udpSocket = SocketUtil::CreateUDPSocket();
		int err = udpSocket->Bind(MyAddress);
	}

	void Leer(Buffer& b)
	{
		char buffer[1300];
		SocketAddress RemoteAddress;
		bool exit = false;
		while (!exit)
		{
			int longDatos = udpSocket->ReceiveFrom(buffer, 1300, RemoteAddress);

			if (longDatos > 0)
			{
				buffer[longDatos] = '\0';
				std::string strBuffer = buffer;
				b.Encolar(strBuffer);
				
				int cmp = strcmp("exit", buffer);
				if (cmp == 0)
				{
					exit = true;
				}
			}
		}
	}
	~Lector() {}
};