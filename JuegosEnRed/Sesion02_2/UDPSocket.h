#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include <Windows.h>
#include "SocketAddress.h"




class UDPSocket
{

private:
	SOCKET mSocket;
	friend class SocketUtil;
	
public:
	UDPSocket(SOCKET inSocket) :mSocket(inSocket) {

	}
	int Bind(const SocketAddress& inToAddress);
	int SendTo(const void* inData, int inLen, const SocketAddress& inTo);
	int ReceiveFrom(void* inBuffer, int inLen, SocketAddress& outFrom);
	~UDPSocket();

};
