#pragma once
#include <string>
#define CHAT_MAX_BUFFER 10

class Buffer
{
private:
	std::string aMensajes[CHAT_MAX_BUFFER];
	int numMensajes;

public:
	Buffer()
	{
		numMensajes = 0;
	}

	void Encolar(std::string mensaje)
	{
		if (numMensajes == CHAT_MAX_BUFFER)
		{
			for (int i = 1; i < CHAT_MAX_BUFFER; i++)
			{
				aMensajes[i - 1] = aMensajes[i];
			}
			aMensajes[CHAT_MAX_BUFFER - 1] = mensaje;
		}
		else
		{
			aMensajes[numMensajes] = mensaje;
			numMensajes++;
		}
	}

	std::string ToString()
	{
		std::string retorno = "";
		for (int i = 0; i < numMensajes; i++)
		{
			retorno += ">>" + aMensajes[i] + "\n";
		}
	}

};