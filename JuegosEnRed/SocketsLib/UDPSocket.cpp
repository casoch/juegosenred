#include "UDPSocket.h"
#include "SocketTools.h"


UDPSocket::UDPSocket():GenericSocket(SOCK_DGRAM)
{
}

int UDPSocket::SendTo(const void * data, int lenData, SocketAddress & to)
{
	sockaddr sa;
	to.GetAddress(sa);
	int numBytesSend = sendto(sock, (const char*)data, lenData, 0, &sa, sizeof(sa));
	if (numBytesSend > 0)
	{
		return numBytesSend;
	}
	else
	{
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketTools::MostrarError("UDPSocket::SendTo");
		return -WSAGetLastError();
	}
}

int UDPSocket::ReceiveFrom(void * data, int lenData, SocketAddress & from)
{
	sockaddr sa;
	int sizeFrom = sizeof(sockaddr);
	int numBytesReceived = recvfrom(sock, (char*)data, lenData, 0, &sa, &sizeFrom);
	if (numBytesReceived >= 0)
	{
		from.SetAddress(sa);
		return numBytesReceived;
	}
	else
	{
		int error = WSAGetLastError();

		if (error == WSAEWOULDBLOCK)
		{
			return 0;
		}
		else if (error == WSAECONNRESET)
		{
			//this can happen if a client closed and we haven't DC'd yet.
			//this is the ICMP message being sent back saying the port on that computer is closed
			return -WSAECONNRESET;
		}
		else
		{
			SocketTools::MostrarError("UDPSocket::ReceiveFrom");
			return -error;
		}
	}
}


UDPSocket::~UDPSocket()
{
	
}
