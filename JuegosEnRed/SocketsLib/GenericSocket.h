#pragma once
#include "SocketAddress.h"

class GenericSocket
{
protected:
	SOCKET sock;
public:
	GenericSocket(int type);
	GenericSocket(SOCKET _sock);
	int Bind(SocketAddress& sa);
	int NonBlocking(bool esNonBlocking);
	virtual ~GenericSocket();
};

