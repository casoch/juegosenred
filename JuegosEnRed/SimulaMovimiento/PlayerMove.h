#pragma once
class PlayerMove
{
private:
	int position;
	int idSquare;
	bool fire;
public:
	PlayerMove();
	PlayerMove(int _position, int _idSquare, bool fire);
	PlayerMove(const PlayerMove& _playerMove);
	int GetPosition();
	int GetIdSquare();
	bool GetFire();
	void SetPosition(int _position);
	void SetIdSquare(int _idSquare);
	void SetFire(bool _fire);
	~PlayerMove();
};

