#pragma once
#include <vector>
#include "PlayerMove.h"

class PlayerMoveList : public std::vector<PlayerMove>
{
public:
	PlayerMoveList();
	void AddMove(int _position, int _idSquare, bool _fire);
	void AddMoves(int _idSquare, std::vector<int> _aPositions, int _lastPosition, bool _fire);
	bool PopMove(PlayerMove& _playerMove);
	int GetLastPosition(int _idSquare);
	~PlayerMoveList();
};

