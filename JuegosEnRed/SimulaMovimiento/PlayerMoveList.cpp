#include "PlayerMoveList.h"



PlayerMoveList::PlayerMoveList()
{
}

void PlayerMoveList::AddMove(int _position, int _idSquare, bool _fire)
{
	PlayerMove playerMove(_position, _idSquare, _fire);
	push_back(playerMove);
}

void PlayerMoveList::AddMoves(int _idSquare, std::vector<int> _aPositions, int _lastPosition, bool _fire)
{
	for (size_t i = 0; i < _aPositions.size(); i++)
	{
		AddMove(_aPositions[i], _idSquare, _fire);
	}
	if (_aPositions.size() == 0 && _fire)
	{
		AddMove(_lastPosition, _idSquare, _fire);
	}
}

bool PlayerMoveList::PopMove(PlayerMove& _playerMove)
{
	if (size() == 0)
	{
		return false;
	}
	_playerMove = at(0);
	erase(begin(), begin() + 1);
	return true;
}

int PlayerMoveList::GetLastPosition(int _idSquare)
{
	int position = -1;
	for (int i = size() - 1; i >= 0; i--)
	{
		PlayerMove pm = at(i);
		if (pm.GetIdSquare() == _idSquare)
		{
			position = pm.GetPosition();
			break;
		}
	}
	return position;
}


PlayerMoveList::~PlayerMoveList()
{
}
