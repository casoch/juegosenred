#include "PlayerMove.h"


PlayerMove::PlayerMove():idSquare(0)
{
}

PlayerMove::PlayerMove(int _position, int _idSquare, bool _fire): position(_position), idSquare(_idSquare), fire(_fire)
{
}

PlayerMove::PlayerMove(const PlayerMove & _playerMove):PlayerMove(_playerMove.position, _playerMove.idSquare, _playerMove.fire)
{

}

int PlayerMove::GetPosition()
{
	return position;
}

int PlayerMove::GetIdSquare()
{
	return idSquare;
}

bool PlayerMove::GetFire()
{
	return fire;
}

void PlayerMove::SetPosition(int _position)
{
	position = _position;
}

void PlayerMove::SetIdSquare(int _idSquare)
{
	idSquare = _idSquare;
}

void PlayerMove::SetFire(bool _fire)
{
	fire = _fire;
}

PlayerMove::~PlayerMove()
{
}
