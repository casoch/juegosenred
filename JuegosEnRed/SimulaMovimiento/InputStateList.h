#pragma once
#include "InputState.h"
#include <vector>
#include <iostream>

class InputStateList: private std::vector<InputState>
{
private:
	int counter;
public:
	InputStateList():counter(0) {}
	
	void Add(InputState& _inputState)
	{
		counter++;
		push_back(_inputState);
	}

	int GetCounter()
	{
		return counter;
	}

	int Size()
	{
		return size();
	}

	bool Acknowledge(int _id, int _ackPosition)
	{
		bool okAck = true;
		int posRemove = -1;
		
		for (int i = 0; i < size(); i++)
		{
			InputState inputState = this->at(i);
			if (_id == inputState.GetId())
			{
				posRemove = i;
				if (inputState.GetAbsolutePosition() != _ackPosition)
				{
					//std::cout << ">>>>>>>>Corrijo de " << inputState.GetAbsolutePosition() << " a " << _ackPosition << std::endl;
					if (posRemove + 2 < size())
					{
						erase(begin() + posRemove + 1, begin() + size() - posRemove - 1);
					}
					okAck = false;
				}
				else
				{
					if (posRemove > 0)
					{
						erase(begin(), begin() + posRemove);
					}
				}
				
				break;
			}
		}
		return okAck;
	}
};