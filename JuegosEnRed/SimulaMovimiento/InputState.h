#pragma once
#include <string>
#include <vector>
#include "GameClientConstants.h"


class InputState
{
private:
	int absolutePosition;
	std::vector<int> aMoves;
	int id;
	bool fire;
public:
	InputState():absolutePosition(0),id(0),fire(false)
	{
		
	}

	InputState(const InputState& _inputState)
	{
		aMoves = _inputState.aMoves;
		id = _inputState.id;
		absolutePosition = _inputState.absolutePosition;
		fire = _inputState.fire;
	}

	int GetId()
	{
		return id;
	}

	void SetId(int _id)
	{
		id = _id;
	}

	int GetDelta()
	{
		int delta = 0;
		for (size_t i = 0; i < aMoves.size(); i++)
		{
			delta += aMoves[i];
		}
		return delta;
	}

	int GetAbsolutePosition()
	{
		return absolutePosition;
	}

	void SetAbsolutePosition(int _absolutePosition)
	{
		absolutePosition = _absolutePosition;
	}

	bool GetFire()
	{
		return fire;
	}

	void SetFire(bool _fire)
	{
		fire = _fire;
	}

	void AddLeft()
	{
		aMoves.push_back(-1);
	}

	void AddRight()
	{
		aMoves.push_back(1);
	}

	std::vector<int> GetMoves()
	{
		return aMoves;
	}

	void ResetMove()
	{
		aMoves.clear();
		fire = false;
	}

	~InputState() {}
};

