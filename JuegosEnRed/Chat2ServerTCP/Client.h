#pragma once
#include "SocketAddress.h"
#include "TCPSocket.h"
#include <string>

class Client
{
private: 
	std::string _nick;
	SocketAddress _address;
	TCPSocketPtr _tcpSocket;
public:
	Client(std::string nick, SocketAddress& address, TCPSocketPtr tcpSocket);
	bool Send(std::string mensaje);
	std::string Receive();
	inline bool operator == (SocketAddress& address);
	~Client();
};

