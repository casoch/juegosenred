#pragma once


class SocketAddressFactory
{
public:
	static SocketAddressPtr CreateIPv4FromString(const std::string& inString)
	{
		/*inString.find_last_of
		The position of the last character that matches.
		If no matches are found, the function returns string::npos.*/
		size_t pos = inString.find_last_of(':');
		std::string host, service;

		//npos is a static member constant value with the greatest possible value for an element of type size_t.
		//This constant is defined with a value of - 1, which because size_t is an unsigned integral type, it is the largest possible representable value for this type.
		if (pos != std::string::npos)
		{
			host = inString.substr(0, pos);
			service = inString.substr(pos + 1);
		}
		else
		{
			host = inString;
			//default port
			service = "0";
		}

		addrinfo hint;
		memset(&hint, 0, sizeof(hint));
		hint.ai_family = AF_INET;

		addrinfo* result;
		int error = getaddrinfo(host.c_str(), service.c_str(), &hint, &result);

		if (error != 0 && result != nullptr)
		{
			freeaddrinfo(result);
			return nullptr;
		}

		while (!result->ai_addr && result->ai_next)
		{
			result = result->ai_next;
		}
		if (!result->ai_addr)
		{
			freeaddrinfo(result);
			return nullptr;
		}

		SocketAddressPtr toRet = std::make_shared<SocketAddress>(*result->ai_addr);
		toRet->puertoOK();
		freeaddrinfo(result);

		return toRet;
		return nullptr;
	}

};
