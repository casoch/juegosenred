#include "Client.h"
#include "SocketUtil.h"



Client::Client(std::string nick, SocketAddress& address, TCPSocketPtr tcpSocket): _nick(nick), _address(address), _tcpSocket(tcpSocket)
{
	
}

bool Client::Send(std::string mensaje)
{
	char datos[1300];
	
	int sendBytes = _tcpSocket->Send(mensaje.c_str(), mensaje.size());
	if (sendBytes > 0)
	{
		return true;
	}
	else
	{
		SocketUtil::ReportError("ERROR: Client::enviar");
		return false;
	}
}

std::string Client::Receive()
{
	char cadena[1300];
	int longDatos = _tcpSocket->Receive(cadena, 1300);

	if (longDatos > 0)
	{
		cadena[longDatos] = '\0';
	}
	std::string ret = cadena;
	return ret;
}

inline bool Client::operator==(SocketAddress & address)
{
	bool iguales = _address == address;
	return iguales;
}


Client::~Client()
{
	
}
