#include "GameObject.h"



GameObject::GameObject()
{
}

GameObject::GameObject(int x, int y)
{
	coord.first = x;
	coord.second = y;
}

void GameObject::GetCoord(int & x, int & y)
{
	x = coord.first;
	y = coord.second;
}

void GameObject::SetCoord(int x, int y)
{
	coord.first = x;
	coord.second = y;
}

bool GameObject::operator==(GameObject & gameObject)
{
	if (gameObject.coord.first == coord.first && gameObject.coord.second == coord.second)
		return true;
	return false;
}


GameObject::~GameObject()
{
}
