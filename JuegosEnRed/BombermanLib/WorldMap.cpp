#include "WorldMap.h"
#include <fstream>
#include <utility>

bool WorldMap::ExplodingBombCell(std::pair<int, int> origin, std::vector<std::pair<int, int>>& aExplodingCells)
{
	if (origin.first >= NUM_ROWS || origin.first < 0 || origin.second >= NUM_COLS || origin.second < 0)
	{
		return false;
	}
	else
	{
		Cell cell = worldMap2D[origin.first][origin.second];
		int idSprite = cell.GetIdSprite();
		if (idSprite == STRONG_WALL)
			return false;
		else if (idSprite == WEAK_WALL || idSprite == FLOOR)
		{
			aExplodingCells.push_back(std::pair<int, int>(origin.first, origin.second));
			if (idSprite == WEAK_WALL)
			{
				//El weak wall explota y pasa a ser suelo.
				worldMap2D[origin.first][origin.second].SetSprite(FLOOR);
				int numRandom = std::rand() % 5;
				if (numRandom == 0)
				{
					Bonus bonus(origin.first, origin.second);
					aBonus.push_back(bonus);
				}
			}
			else if (idSprite == FLOOR)
			{
				for (size_t i = 0; i < NUM_PLAYERS; i++)
				{
					int x, y;
					aPlayers[i].GetCoord(x, y);
					if (origin.first == x && origin.second == y)
					{
						aPlayers[i].SubstractLife();
					}
				}
			}
			return idSprite == FLOOR;
		}
	}
}

WorldMap::WorldMap()
{
	
}

void WorldMap::LoadFromFile(std::string nameFile)
{
	std::ifstream myFile;
	std::string path = "../Bomberman/";
	path = path.append(nameFile);

	myFile.open(nameFile);
	if (myFile.is_open())
	{
		int numPlayer = 0;
		std::string line;
		for (size_t i = 0; i < NUM_ROWS; i++)
		{
			myFile >> line;
			for (size_t j = 0; j < NUM_COLS; j++)
			{
				char c = line.at(j);
				switch (c)
				{
				case 'F':
					worldMap2D[i][j].SetSprite(FLOOR);
					break;
				case 'S':
					worldMap2D[i][j].SetSprite(STRONG_WALL);
					break;
				case 'W':
					worldMap2D[i][j].SetSprite(WEAK_WALL);
					break;
				case 'P':
					worldMap2D[i][j].SetSprite(FLOOR);
					aPlayers[numPlayer].SetCoord(i, j);
					numPlayer++;
					break;
				}
			}
		}
		myFile.close();
	}
	
}

bool WorldMap::MovePlayer(int idPlayer)
{
	std::pair<int, int> newPos = aPlayers[idPlayer].Go();
	if (newPos.first < 0 || newPos.second < 0 || newPos.first >= NUM_ROWS || newPos.second >= NUM_COLS)
	{
		return false;
	}
	else
	{
		Cell cell = worldMap2D[newPos.first][newPos.second];
		int idSpriteCell = cell.GetIdSprite();
		if (idSpriteCell != FLOOR)
		{
			return false;
		}
		else
		{
			//Es suelo, puedo caminar ah�.
			//He de comprobar que no haya un Bonus. Si hay un bonus se me incrementa la vida.
			for (int i = 0; i < aBonus.size(); i++)
			{
				int x, y;
				Bonus bonus = aBonus[i];
				bonus.GetCoord(x, y);
				if (x == newPos.first && y == newPos.second)
				{
					aPlayers[idPlayer].AddLife();
					aBombs.erase(aBombs.begin() + i);
					return true;
				}
			}
		}
	}
	return false;
}

void WorldMap::RotatePlayer(int idPlayer, bool left)
{
	if (left)
		aPlayers[idPlayer].RotateLeft();
	else
		aPlayers[idPlayer].RotateRight();
}

void WorldMap::ShootBomb(int idPlayer)
{
	int x, y;
	aPlayers[idPlayer].GetCoord(x, y);
	Bomb bomb(x, y);
	aBombs.push_back(bomb);

}

std::vector<std::pair<int,int>> WorldMap::ExplodingBombs()
{
	std::vector<std::pair<int, int>> aExplodingCells;
	for (size_t i = 0; i < aBombs.size(); i++)
	{
		Bomb bomb = aBombs[i];
		if (bomb.ExplodeNow())
		{
			int x, y;
			bomb.GetCoord(x, y);
			//La casilla donde est� la bomba, explosiona
			aExplodingCells.push_back(std::pair<int, int>(x, y));

			//Si hay un strong wall, para la explosi�n
			//Si hay un weak wall, explosiona y para la explosi�n.
			//Para el resto de casos, la explosi�n se propaga

			//Explosionan 3 casillas hacia la derecha
			for (int inc = 1; inc <= 3; inc++)
			{
				bool continueExploding = ExplodingBombCell(std::pair<int, int>(x + inc, y), aExplodingCells);
				if (!continueExploding)
				{
					break;
				}
			}

			//Explosionan 3 casillas hacia la izquierda
			for (int inc = 1; inc <= 3; inc++)
			{
				bool continueExploding = ExplodingBombCell(std::pair<int, int>(x - inc, y), aExplodingCells);
				if (!continueExploding)
				{
					break;
				}
			}

			//Explosionan 3 casillas hacia arriba
			for (int inc = 1; inc <= 3; inc++)
			{
				bool continueExploding = ExplodingBombCell(std::pair<int, int>(x, y - inc), aExplodingCells);
				if (!continueExploding)
				{
					break;
				}
			}

			//Explosionan 3 casillas hacia abajo
			for (int inc = 1; inc <= 3; inc++)
			{
				bool continueExploding = ExplodingBombCell(std::pair<int, int>(x, y + inc), aExplodingCells);
				if (!continueExploding)
				{
					break;
				}
			}
		}
		else
		{
			bomb.SubstractSeconds();
		}
	}
	return aExplodingCells;
}

std::string WorldMap::SerializeWorldMap2D()
{
	std::string serialized = "";
	for (size_t i = 0; i < NUM_ROWS; i++)
	{
		for (size_t j = 0; j < NUM_COLS; j++)
		{
			Cell cell = worldMap2D[i][j];
			if (cell.GetIdSprite() == FLOOR)
			{
				serialized = serialized.append("F");
			}
			else if (cell.GetIdSprite() == WEAK_WALL)
			{
				serialized = serialized.append("W");
			}
			else if (cell.GetIdSprite() == STRONG_WALL)
			{
				serialized = serialized.append("S");
			}
		}
	}
	return serialized;
}

void WorldMap::DeserializeWorldMap2D(std::string buffer)
{
	int posBuffer = 0;
}

std::string WorldMap::SerializePlayers()
{
	std::string serialize = "";
	for (size_t i = 0; i < NUM_PLAYERS; i++)
	{
		serialize = serialize.append(aPlayers[i].Serialize());
		if (i + 1 < NUM_PLAYERS)
		{
			serialize = serialize.append("#");
		}
	}
	return serialize;
}

std::string WorldMap::SerializeBombs()
{
	std::string serialize = "";
	for (size_t i = 0; i < aBombs.size(); i++)
	{
		serialize = serialize.append(aBombs[i].Serialize());
		if (i + 1 < aBombs.size())
		{
			serialize = serialize.append("#");
		}
	}
	return serialize;
}

std::string WorldMap::SerializeBonus()
{
	std::string serialize = "";
	for (size_t i = 0; i < aBonus.size(); i++)
	{
		serialize = serialize.append(aBonus[i].Serialize());
		if (i + 1 < aBonus.size())
		{
			serialize = serialize.append("#");
		}
	}
	return serialize;
}

void WorldMap::SetNamePlayer(int idPlayer, std::string name)
{
	aPlayers[idPlayer].SetName(name);
}


WorldMap::~WorldMap()
{
	
}
