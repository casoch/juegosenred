#pragma once
#include <utility>
#include <string>
class GameObject
{
protected:
	std::pair<int, int> coord;
public:
	GameObject();
	GameObject(int x, int y);
	void GetCoord(int& x, int& y);
	void SetCoord(int x, int y);
	bool operator==(GameObject& gameObject);
	virtual std::string Serialize()=0;
	virtual void Deserialize(std::string buffer) = 0;
	~GameObject();
};

