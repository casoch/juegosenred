#pragma once
class Cell
{
private:
	int idSprite;
public:
	Cell();
	Cell(int idSprite);
	void SetSprite(int idSprite);
	int GetIdSprite();
	~Cell();
};

