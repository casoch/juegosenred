#pragma once
#include "GameObject.h"

class Bomb :
	public GameObject
{
	int secondsToExplode;
public:
	Bomb();
	Bomb(int x, int y);
	bool ExplodeNow();
	void SubstractSeconds();
	std::string Serialize();
	void Deserialize(std::string buffer);
	~Bomb();
};

