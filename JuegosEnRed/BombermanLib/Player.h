#pragma once
#include "GameObject.h"
#include "Bomb.h"

#define ANGLE_RIGHT 0
#define ANGLE_UP 90
#define ANGLE_LEFT 180
#define ANGLE_DOWN 270
#define INC_ANGLE 90

class Player :
	public GameObject
{
private:
	std::string name;
	int numLifes;
	int angle;
public:
	Player();
	Player(std::string name);
	void SubstractLife();
	void AddLife();
	void RotateRight();
	void RotateLeft();
	std::pair<int, int> Go();
	int GetAngle();
	int GetNumLifes();
	void SetName(std::string name);
	bool isDead();
	std::string GetName();
	Bomb ShootBomb();
	std::string Serialize();
	void Deserialize(std::string buffer);
	~Player();
};

