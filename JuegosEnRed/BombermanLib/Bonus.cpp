#include "Bonus.h"



Bonus::Bonus()
{
}

Bonus::Bonus(int x, int y):GameObject(x, y)
{
}

std::string Bonus::Serialize()
{
	std::string serialized;
	serialized = serialized.append(std::to_string(coord.first));
	serialized = serialized.append("|");
	serialized = serialized.append(std::to_string(coord.second));
	return serialized;
}

void Bonus::Deserialize(std::string buffer)
{
	int indexPipe = buffer.find_first_of("|");
	std::string strCoordX = buffer.substr(0, indexPipe - 1);
	int x = atoi(strCoordX.c_str());

	buffer = buffer.substr(indexPipe, buffer.size() - indexPipe);
	std::string strCoordY = buffer;
	int y = atoi(strCoordY.c_str());

	SetCoord(x, y);

}

Bonus::~Bonus()
{
}
