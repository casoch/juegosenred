#include "Bomb.h"



Bomb::Bomb()
{
	secondsToExplode = 3;
}

Bomb::Bomb(int x, int y):GameObject(x, y)
{
	secondsToExplode = 3;
}

bool Bomb::ExplodeNow()
{
	return secondsToExplode == 0;
}

void Bomb::SubstractSeconds()
{
	secondsToExplode--;
}

std::string Bomb::Serialize()
{
	std::string serialized;
	serialized = serialized.append(std::to_string(coord.first));
	serialized = serialized.append("|");
	serialized = serialized.append(std::to_string(coord.second));
	serialized = serialized.append("|");
	serialized = serialized.append(std::to_string(secondsToExplode));
	return serialized;
}

void Bomb::Deserialize(std::string buffer)
{
	int indexPipe = buffer.find_first_of("|");
	std::string strCoordX = buffer.substr(0, indexPipe - 1);
	int x = atoi(strCoordX.c_str());

	buffer = buffer.substr(indexPipe, buffer.size() - indexPipe);
	indexPipe = buffer.find_first_of("|");
	std::string strCoordY = buffer.substr(0, indexPipe - 1);
	int y = atoi(strCoordY.c_str());

	std::string strSeconds = buffer;
	int seconds = atoi(strSeconds.c_str());

	SetCoord(x, y);
	this->secondsToExplode = seconds;
}

Bomb::~Bomb()
{
}
