#pragma once
#include "GameObject.h"
#include "Player.h"
#include <string>
#include "Cell.h"
#include "Bomb.h"
#include <vector>
#include "Bonus.h"


#define NUM_ROWS 11
#define NUM_COLS 13
#define NUM_PLAYERS 4
#define FLOOR 0
#define STRONG_WALL 1
#define WEAK_WALL 2


class WorldMap
{
private:
	Cell worldMap2D[NUM_ROWS][NUM_COLS];
	Player aPlayers[NUM_PLAYERS];
	std::vector<Bomb> aBombs;
	std::vector<Bonus> aBonus;

	bool ExplodingBombCell(std::pair<int, int> origin, std::vector<std::pair<int, int>>& aExplodingCells);
public:
	WorldMap();
	void LoadFromFile(std::string path);
	std::string SerializeWorldMap2D();
	std::string SerializePlayers();
	std::string SerializeBombs();
	std::string SerializeBonus();
	void DeserializeWorldMap2D(std::string buffer);
	void DeserializePlayers(std::string buffer);
	void DeserializeBombs(std::string buffer);
	void DeserializeBonus(std::string buffer);
	void SetNamePlayer(int idPlayer, std::string name);
	bool MovePlayer(int idPlayer);
	void RotatePlayer(int idPlayer, bool left);
	void ShootBomb(int idPlayer);
	
	std::vector<std::pair<int, int>> ExplodingBombs();
	~WorldMap();
};

