#pragma once
#include <vector>

template <class T>
class List : private std::vector<T>
{
public:
	List() {}
	void Add(T element)
	{
		push_back(element);
	}

	int IndexOf(T element)
	{
		for (size_t i = 0; i < size(); i++)
		{
			if (at(i) == element)
			{
				return i;
			}
		}
		return -1;
	}

	void Remove(int position)
	{
		if (position < size())
			erase(begin() + position);
	}

	void Remove(T element)
	{
		int position = IndexOf(element);
		Remove(position);
	}

	~List()	{}
};