#include "Player.h"

Player::Player()
{
	numLifes = 5;
	angle = ANGLE_RIGHT;
}

Player::Player(std::string name):Player()
{
	this->name = name;
}

void Player::AddLife()
{
	numLifes++;
}

void Player::SubstractLife()
{
	numLifes--;
}

void Player::RotateRight()
{
	angle += INC_ANGLE;
	angle = angle % 360;
}

void Player::RotateLeft()
{
	angle -= INC_ANGLE;
	if (angle < 0)
	{
		angle = 360 + angle;
	}
	angle = angle % 360;
}

std::pair<int, int> Player::Go()
{
	std::pair<int, int> newCoord(coord);
	switch (angle)
	{
	case ANGLE_RIGHT:
		newCoord.first++;
		break;
	case ANGLE_UP:
		newCoord.second--;
		break;
	case ANGLE_LEFT:
		newCoord.first--;
		break;
	case ANGLE_DOWN:
		newCoord.second++;
		break;
	}
	return newCoord;
}


int Player::GetAngle()
{
	return angle;
}

int Player::GetNumLifes()
{
	return numLifes;
}

bool Player::isDead()
{
	return numLifes==0;
}

std::string Player::GetName()
{
	return std::string();
}

Bomb Player::ShootBomb()
{
	int x, y;
	GetCoord(x, y);
	Bomb b(x, y);
	return b;
}

std::string Player::Serialize()
{
	std::string serialized;
	serialized = serialized.append(std::to_string(coord.first));
	serialized = serialized.append("|");
	serialized = serialized.append(std::to_string(coord.second));
	serialized = serialized.append("|");
	serialized = serialized.append(name);
	serialized = serialized.append("|");
	serialized = serialized.append(std::to_string(numLifes));
	serialized = serialized.append("|");
	serialized = serialized.append(std::to_string(angle));
	return serialized;
}

void Player::Deserialize(std::string buffer)
{
	int indexPipe = buffer.find_first_of("|");
	std::string strCoordX = buffer.substr(0, indexPipe - 1);
	int x = atoi(strCoordX.c_str());

	buffer = buffer.substr(indexPipe, buffer.size() - indexPipe);
	indexPipe = buffer.find_first_of("|");
	std::string strCoordY = buffer.substr(0, indexPipe - 1);
	int y = atoi(strCoordY.c_str());

	buffer = buffer.substr(indexPipe, buffer.size() - indexPipe);
	indexPipe = buffer.find_first_of("|");
	std::string name = buffer.substr(0, indexPipe - 1);
	
	buffer = buffer.substr(indexPipe, buffer.size() - indexPipe);
	indexPipe = buffer.find_first_of("|");
	std::string strNumLifes = buffer.substr(0, indexPipe - 1);
	int numLifes = atoi(strNumLifes.c_str());

	std::string strAngle = buffer;
	int angle = atoi(strAngle.c_str());

	SetCoord(x, y);
	this->name = name;
	this->numLifes = numLifes;
	this->angle = angle;
}

void Player::SetName(std::string name)
{
	this->name = name;
}

Player::~Player()
{
}
