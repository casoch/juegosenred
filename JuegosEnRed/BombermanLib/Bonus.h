#pragma once
#include "GameObject.h"
class Bonus :
	public GameObject
{
public:
	Bonus();
	Bonus(int x, int y);
	std::string Serialize();
	void Deserialize(std::string buffer);
	~Bonus();
};

