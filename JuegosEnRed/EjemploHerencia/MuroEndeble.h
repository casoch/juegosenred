#pragma once
#include "Muro.h"
#include <string>

class MuroEndeble :
	public Muro
{
private:
	std::string objetoOculto;
	bool visible;
public:
	MuroEndeble();
	MuroEndeble(int posX, int posY, 
		std::string objetoOculto);
	MuroEndeble(MuroEndeble& muro);
	void dibuja();
	std::string explosionar();
	~MuroEndeble();
};

