#include "Muro.h"
#include <iostream>


Muro::Muro()
{
	posX = 0;
	posY = 0;
}

Muro::Muro(int posX, int posY)
{
	this->posX = posX;
	this->posY = posY;
}

Muro::Muro(Muro & muro):Muro(muro.posX, muro.posY)
{
	
}

void Muro::dibuja()
{
	std::cout << "Muro::dibuja" << std::endl;
}


Muro::~Muro()
{
	std::cout << "Muro::Destructor" << std::endl;
}
