#include "MuroFijo.h"
#include <iostream>


std::string MuroFijo::explosionar()
{
	//Tenemos la funci�n que no hace nada y es privada para no poderla
	//llamar desde fuera
	return "";
}

MuroFijo::MuroFijo()
{
}

MuroFijo::MuroFijo(int posX, int posY):Muro(posX, posY)
{
	
}

MuroFijo::MuroFijo(MuroFijo & muro):Muro(muro)
{

}


MuroFijo::~MuroFijo()
{
	std::cout << "MuroFijo::Destructor" << std::endl;
}
