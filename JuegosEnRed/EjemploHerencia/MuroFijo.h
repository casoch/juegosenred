#pragma once
#include "Muro.h"
class MuroFijo :
	public Muro
{
public:
	MuroFijo();
	MuroFijo(int posX, int posY);
	MuroFijo(MuroFijo& muro);
	std::string explosionar();
	~MuroFijo();
};

