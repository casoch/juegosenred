#include "MuroEndeble.h"
#include <iostream>


MuroEndeble::MuroEndeble()
{
	visible = true;
	objetoOculto = "";
}

MuroEndeble::MuroEndeble(int posX, int posY, 
	std::string objetoOculto):Muro(posX, posY)
{
	this->objetoOculto = objetoOculto;
	visible = true;
}

MuroEndeble::MuroEndeble(MuroEndeble & muro):Muro(muro)
{
	this->objetoOculto = muro.objetoOculto;
	this->visible = muro.visible;
}

void MuroEndeble::dibuja()
{
	if (visible)
	{
		//dibuja
	}
}

std::string MuroEndeble::explosionar()
{
	visible = false;
	return objetoOculto;
}


MuroEndeble::~MuroEndeble()
{
	std::cout << "MuroEndeble::Destructor" << std::endl;
}
