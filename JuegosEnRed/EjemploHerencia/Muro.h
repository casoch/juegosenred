#pragma once
#include <string>
class Muro
{
private:
	int posX, posY;
public:
	Muro();
	Muro(int posX, int posY);
	Muro(Muro& muro);
	virtual void dibuja();
	virtual std::string explosionar() = 0;
	virtual ~Muro();
};

