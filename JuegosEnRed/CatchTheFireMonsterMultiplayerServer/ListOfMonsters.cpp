#include "ListOfMonsters.h"
#include <time.h>

/*
* Constructor
* @param n is the number of monsters to store
*/
ListOfMonsters::ListOfMonsters() {
	
}

/*
* Destructor
*/
ListOfMonsters::~ListOfMonsters(){
	
}

int ListOfMonsters::size()
{
	return aMonsters.size();
}

bool ListOfMonsters::checkIfSomeMonsterDies(std::pair<int, int> coord)
{
	for (int i = 0; i < size(); i++) {
		if (coord.first >= aMonsters[i].first && coord.first <= aMonsters[i].first + MONSTER_DEFAULT_WIDTH &&
			coord.second >= aMonsters[i].second && coord.second <= aMonsters[i].second + MONSTER_DEFAULT_HEIGHT)
		{
			aMonsters.erase(aMonsters.begin() + i);
			i--;
		}
	}
	return false;
}

void ListOfMonsters::ReInitMonsters()
{
	int sizeAnt = size();
	InitMonsters(sizeAnt);
}

void ListOfMonsters::InitMonsters(int length)
{
	
	aMonsters.clear();
	for (size_t i = 0; i < length; i++)
	{
		int x = rand() % (int)(SCREEN_WIDTH*0.9f);
		int y = rand() % (int)(SCREEN_HEIGHT*0.9f);
		aMonsters.push_back(std::pair<int, int>(x, y));
	}
}

std::string ListOfMonsters::Serialize()
{
	int numMonsters = size();
	if (numMonsters == 0)
	{
		return "";
	}

	std::string ret = "MONSTERS_";
	ret = ret.append(std::to_string(numMonsters));
	ret = ret.append("#");
	for (size_t i = 0; i < numMonsters; i++)
	{
		std::pair<int, int> aPair = aMonsters[i];
		ret = ret.append(std::to_string(aPair.first));
		ret = ret.append(":");
		ret = ret.append(std::to_string(aPair.second));
		if (i + 1 < size())
		{
			ret = ret.append("#");
		}
	}
	return ret;
}

