#include "ClientProxy.h"



ClientProxy::ClientProxy()
{
}

ClientProxy::ClientProxy(const std::string nick, const SocketAddress & sa)
{
	this->nick = nick;
	clientIPPort = sa;
}

ClientProxy::ClientProxy(const ClientProxy & clientProxy):ClientProxy(clientProxy.nick, clientProxy.clientIPPort)
{
}

bool ClientProxy::operator==(ClientProxy & clientProxy)
{
	sockaddr_in _sa_in, sa_in;
	clientProxy.clientIPPort.GetAddress(_sa_in);
	clientIPPort.GetAddress(sa_in);
	if (_sa_in.sin_addr.S_un.S_addr == sa_in.sin_addr.S_un.S_addr && _sa_in.sin_port == sa_in.sin_port && nick == clientProxy.nick)
	{
		return true;
	}
	return false;
}

bool ClientProxy::Contains(std::vector<ClientProxy>& aJugadores, ClientProxy & clientProxy)
{
	for (size_t i = 0; i < aJugadores.size(); i++)
	{
		if (aJugadores[i] == clientProxy)
		{
			return true;
		}
	}
	
	return false;
}


ClientProxy::~ClientProxy()
{
}
