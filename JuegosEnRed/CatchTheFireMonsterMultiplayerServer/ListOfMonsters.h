#pragma once
#include <string>
#include "GameConstants.h"
#include <vector>

class ListOfMonsters
{
	std::vector<std::pair<int, int>> aMonsters;
public:
	ListOfMonsters();
	~ListOfMonsters();
	int size();
	bool checkIfSomeMonsterDies(std::pair<int, int> coord);

	void ReInitMonsters();
	
	//void setRandomPosition(int screenWidth, int screenHeight);
	void InitMonsters(int length);

	std::string Serialize();
	
};

