#pragma once
#include <SocketAddress.h>
#include <vector>
class ClientProxy
{
private:
	SocketAddress clientIPPort;
	std::string nick;
	friend class Server;
public:
	ClientProxy();
	ClientProxy(const std::string nick, const SocketAddress& sa);
	ClientProxy(const ClientProxy& clientProxy);
	bool operator==(ClientProxy& clientProxy);
	static bool Contains(std::vector<ClientProxy>& aJugadores, ClientProxy& clientProxy);
	~ClientProxy();
};

