#pragma once
#include <SocketTools.h>
#include "Server.h"
#include "ListOfMonsters.h"
#include <GameConstants.h>
#include <time.h>
#include <UDPSocket.h>

void ProcesarMensaje(Server& servidor, ListOfMonsters& monsters, std::string mensaje)
{
	int index_ = mensaje.find_first_of('_');
	std::string cabecera = mensaje.substr(0, index_);
	std::string contenido = mensaje.substr(index_ + 1, mensaje.size() - index_);
	if (cabecera == "POSITION")
	{
		int index2Ptos = contenido.find_first_of(':');
		std::string strX = contenido.substr(0, index2Ptos);
		std::string strY = contenido.substr(index2Ptos + 1, contenido.size() - index2Ptos);
		int x = atoi(strX.c_str());
		int y = atoi(strY.c_str());
		monsters.checkIfSomeMonsterDies(std::pair<int, int>(x, y));
	}
	
}

void ServerRun(std::string serverAddress)
{
	Server servidor(serverAddress);
	
	servidor.EsperandoJugadores();
	servidor.NonBlocking(true);
	std::cout << "Ponemos a nonblocking" << std::endl;
	servidor.SendToAll("BEGIN");

	ListOfMonsters monsters;
	monsters.InitMonsters(MAX_MONSTERS);
	clock_t lastTimeMonsterWasUpdated = clock();
	do
	{
		if (clock() > lastTimeMonsterWasUpdated + MONSTER_REFRESH_FREQUENCY) 
		{
			monsters.ReInitMonsters();
			std::string strMonsters = monsters.Serialize();
			if (strMonsters == "")
			{
				servidor.SendToAll("END");
				break;
			}
			std::cout << "Env�o monstruos: " <<strMonsters<< std::endl;
			servidor.SendToAll(strMonsters);
			lastTimeMonsterWasUpdated = clock();
		}
		std::string mensaje = servidor.Receive();
		if (mensaje.size() > 0)
		{
			std::cout << "Recibo: " << mensaje << std::endl;
			ProcesarMensaje(servidor, monsters, mensaje);
		}
		
	} while (true);
}




int main(int argc, char** argv) {
	SocketTools::CargarLibreria();
	std::string serverAddress = argv[1];
	ServerRun(serverAddress);
	SocketTools::DescargarLibreria();
	system("pause");
	return 0;
}