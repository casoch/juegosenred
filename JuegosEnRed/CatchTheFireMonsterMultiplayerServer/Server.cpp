#include "Server.h"
#include <GameConstants.h>
#include <string>


Server::Server(std::string serverAddress)
{
	SocketAddress saServer;
	saServer.SetAddress(serverAddress);
	int err = udpSocket.Bind(saServer);
	if (err == -1)
	{
		throw std::exception("Error en Server::Server");
	}
}

bool Server::ProcesarLlegada(char buffer[], std::string& nick)
{
	std::string strBuffer = buffer;
	int index_ = strBuffer.find_first_of('_');
	std::string cabecera = strBuffer.substr(0, index_);
	std::string contenido = strBuffer.substr(index_ + 1, strBuffer.size() - index_);
	if (cabecera == "HELLO")
	{
		nick = contenido;
		return true;
	}
	return false;
}

void Server::EsperandoJugadores()
{
	for (int i = 0; i < NUM_PLAYERS; i++)
	{
		bool okLlegada = false;
		do
		{
			char buffer[MAX_BUFFER];
			SocketAddress from;
			int bytesReceived = udpSocket.ReceiveFrom(buffer, MAX_BUFFER, from);
			
			if (bytesReceived > 0)
			{
				if (bytesReceived < MAX_BUFFER)
				{
					buffer[bytesReceived] = '\0';
				}
				std::string nick;
				okLlegada = ProcesarLlegada(buffer, nick);
				if (okLlegada)
				{
					ClientProxy client(nick, from);
					bool exists = ClientProxy::Contains(aJugadores, client);
					if (!exists)
					{
						aJugadores.push_back(client);
						std::string strWelcome = HEADER_WELCOME;
						Send(strWelcome, from);
						std::cout << "Llega el jugador " << nick.c_str() << std::endl;
					}
				}
				else
				{
					i--;
				}
			}
		} while (!okLlegada);
	}
	
}

int Server::Send(std::string mensaje, SocketAddress& address)
{
	int bytes = udpSocket.SendTo(mensaje.c_str(), mensaje.size(), address);
	return bytes;
}

void Server::SendToAll(std::string mensaje)
{
	for (size_t i = 0; i < aJugadores.size(); i++)
	{
		ClientProxy client = aJugadores[i];
	
		int bytes = Send(mensaje, client.clientIPPort);
		
	}
}

std::string Server::Receive()
{
	std::string ret = "";
	char buffer[MAX_BUFFER];
	SocketAddress from;
	int bytesReceived = udpSocket.ReceiveFrom(buffer, MAX_BUFFER, from);
	
	if (bytesReceived > 0)
	{
		if (bytesReceived < MAX_BUFFER)
		{
			buffer[bytesReceived] = '\0';
		}
		ret = buffer;
	}
	return ret;
}

void Server::NonBlocking(bool ok)
{
	udpSocket.NonBlocking(ok);
	
}


Server::~Server()
{
}
