#include "ErrorManagement.h"
#include <iostream>

/*
* Prints out an error message and exits the game
*/
void ErrorManagement::errorRunTime(std::string errorString) {
	cout << errorString << endl;
	cout << "Program will exit. Enter any key to continue";
	system("pause");
}
