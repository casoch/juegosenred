#pragma once
#include <vector>

#include "ClientProxy.h"
#include <UDPSocket.h>
class Server
{
	std::vector<ClientProxy> aJugadores;
	
	UDPSocket udpSocket;
	bool ProcesarLlegada(char buffer[], std::string& nick);
public:
	Server(std::string serverAddress);
	void EsperandoJugadores();
	void SendToAll(std::string mensaje);
	std::string Receive();
	int Send(std::string mensaje, SocketAddress& address);
	void NonBlocking(bool ok);
	~Server();
};

