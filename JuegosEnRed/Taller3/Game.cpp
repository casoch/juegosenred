#include "Game.h"
#include "SDLInterface.h"

/**
* Constructor
* Tip: You can use an initialization list to set its attributes
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, std::string miDireccion, std::string direccionDestino) :
	_windowTitle(windowTitle),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_gameState(GameState::INIT),
	_network(miDireccion, direccionDestino)

{

}

/**
* Destructor
*/
Game::~Game()
{
}

/*
* Game execution
*/
void Game::run() {
	//Prepare the game components
	init();
	//Start the game if everything is ready
	gameLoop();
}

/*
* Initialize all the components
*/
void Game::init() {
	srand((unsigned int)time(NULL));
	//Create a window
	_graphic.createWindow(_windowTitle, _screenWidth, _screenHeight, false);
	//_graphic.setWindowBackgroundColor(0, 0, 0, 255);

	//Set the font style

	_graphic.setFontStyle(TTF_STYLE_NORMAL);

}


/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {
	_gameState = GameState::PLAY;

	while (_gameState != GameState::EXIT) {
		//Detect keyboard and/or mouse events
		_graphic.detectInputEvents();
		//Execute the player commands 
		//executePlayerCommands();

		//networking();
		//Update the game physics
		//doPhysics();
		//Render game
		renderGame();
	}
}


void Game::networking()
{
	char mensaje[1300];
	int numBytes = _network.Receive(mensaje);
	if (numBytes > 0)
	{
		_buffer.Encolar(mensaje);
	}
}

/**
* Executes the actions sent by the user by means of the keyboard and mouse
* Reserved keys:
- up | left | right | down moves the player object
- m opens the menu
*/
void Game::executePlayerCommands() {

	if (_graphic.isKeyPressed(SDLK_F1))
	{
		_graphic.setBuffering(true);
	}
	if (_graphic.isKeyPressed(SDLK_RETURN) || _graphic.isKeyPressed(SDLK_RETURN2))
	{
		std::string miMsg = _graphic.getBuffer();
		char * cstr = new char[miMsg.length() + 1];
		const char* cstrCpy = miMsg.c_str();
		std::strcpy(cstr, cstrCpy);
		
		_network.Send(cstr);
		_buffer.Encolar(cstr);
		delete[] cstr;
		_graphic.setBuffering(false);
	}
	

	if (_graphic.isKeyPressed(SDLK_ESCAPE)) {
		_gameState = GameState::EXIT;
	}

}

/*
* Execute the game physics
*/
void Game::doPhysics() {
	//Check if player has hit a/some monster/s

	//Update the animation of monsters

	//Update the random positions based on the MONSTER_REFRESH_FREQUENCY

}

/**
* Render the game on the screen
*/
void Game::renderGame() {
	//Clear the screen
	_graphic.clearWindow();

	//Draw the screen's content based on the game state
	//if (_gameState == GameState::MENU) {
	//	drawMenu();
	//}
	//else {
	drawGame();

	//}
	//Refresh screen
	_graphic.refreshWindow();

}

/*
* Draw the game menu
*/
void Game::drawMenu() {

}

/*
* Draw the game, winner or loser screen
*/
void Game::drawGame() {

	//Pintar el juego en s�
	drawRectangle(BLACK, 0, 500, 800, 100);
	//std::string texto = _graphic.getBuffer();
	//_graphic.drawText(texto, BLUE, 1, 550);

	drawRectangle(GREEN, 0, 0, 800, 500);
	//std::vector<std::string> aMsg;
	//_buffer.aMensajes(aMsg);
	//for (int i = 0; i < aMsg.size(); i++)
	//{
	//	_graphic.drawText(aMsg[i], WHITE, 1, 10 * i);
	//}

}



void Game::drawRectangle(int color, int x, int y, int width, int height)
{
	_graphic.drawFilledRectangle(color, x, y, width, height);
}

