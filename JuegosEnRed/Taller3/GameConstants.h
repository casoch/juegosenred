#pragma once

//Game general information
#define GAME_SPEED 0.1f
#define GAME_TEXTURES 8
#define MAX_MONSTERS 10
#define MAX_PLAYERS 2
#define MONSTER_REFRESH_FREQUENCY 500
#define MAX_ROWS_MAP 21
#define MAX_COLUMNS_MAP 19


//Sprite information
#define SPRITE_SPEED 0.01f
#define SPRITE_DEFAULT_WIDTH 32
#define SPRITE_DEFAULT_HEIGHT 32
#define SPRITE_YELLOW_UP 0
#define SPRITE_YELLOW_RIGHT 1
#define SPRITE_YELLOW_DOWN 2
#define SPRITE_YELLOW_LEFT 3
#define SPRITE_RED_UP 4
#define SPRITE_RED_RIGHT 5
#define SPRITE_RED_DOWN 6
#define SPRITE_RED_LEFT 7
#define DIRECTION_UP 8
#define DIRECTION_LEFT 9
#define DIRECTION_DOWN 10
#define DIRECTION_RIGHT 11
#define CASILLA_MURO 0
#define CASILLA_SUELO 1



//Color information
#define GAME_BASIC_COLORS 6
#define RED 0
#define GREEN 1
#define BLUE 2
#define BLACK 3
#define WHITE 4
#define YELLOW 5


//Game has four possible states: INIT (Preparing environment), PLAY (Playing), EXIT (Exit from the game) or MENU (Game menu)
enum class GameState { INIT, PLAY, EXIT, MENU };

