#include "Network.h"
#include "SocketTools.h"

Network::Network(std::string miDireccion, std::string direccionDestino)
{
	SocketAddress mi_sa;
	mi_sa.SetAddress(miDireccion);
	remoteAddress.SetAddress(direccionDestino);
	
	int err = udpSocket.Bind(mi_sa);
}

int Network::Receive(char mensajeRecibido[])
{
	SocketAddress sa;
	int bytesRecibidos = udpSocket.ReceiveFrom((char*)mensajeRecibido, 1300, sa);
	if (bytesRecibidos > 0)
	{
		mensajeRecibido[bytesRecibidos] = '\0';
	}
	else
	{
		SocketTools::MostrarError("Error en Network::Receive");
	}
	return bytesRecibidos;
	
}

int Network::Send(char mensajeAEnviar[])
{
	int bytesEnviados = udpSocket.SendTo(mensajeAEnviar, 100, remoteAddress);
	if (bytesEnviados < 0)
	{
		SocketTools::MostrarError("Error en Network::Send");
	}
	return bytesEnviados;
}

Network::~Network()
{
}



