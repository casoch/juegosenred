#include "Game.h"
#include "SocketTools.h"
#include <map>
#include <cstring>


int main(int argc, char ** argv)
{
	std::string funcion = argv[1];
	std::string direccionRecepcion = argv[2];
	std::string direccionEnvio = argv[3];

	SocketTools::CargarLibreria();
	
	if (funcion == "servidor")
	{

	}
	else if (funcion == "cliente")
	{
		Game game("The naive walking", 800, 600, direccionRecepcion, direccionEnvio);

		try {
			game.run();
		}
		catch (std::exception e) {
			std::cerr << "ERROR: " << e.what() << std::endl;
		}

	}

	SocketTools::DescargarLibreria();
	return 0;
}