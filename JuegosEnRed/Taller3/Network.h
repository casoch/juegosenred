#pragma once
#include "UDPSocket.h"

class Network
{
private:
	UDPSocket udpSocket;
	SocketAddress remoteAddress;

public:
	Network(std::string miDireccion, std::string direccionDestino);
	int Receive(char mensajeRecibido[]);
	int Send(char mensajeAEnviar[]);
	~Network();
};

