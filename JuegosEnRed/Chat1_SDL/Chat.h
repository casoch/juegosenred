#pragma once

//Third-party libraries
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <iostream>
#include <time.h>
#include "GameConstants.h"
#include "SDLInterface.h"
#include "InputManager.h"
#include "Network.h"
#include "Buffer.h"



/*
* The Game class manages the game execution
*/
class Chat {
public:
	Chat(std::string windowTitle, int screenWidth, int screenHeight, std::string myAdr, uint16_t myPort, std::string remAdr, uint16_t remPort);	//Constructor
	~Chat();															//Destructor
	void run();															//Game execution	

private:
	//Attributes	
	std::string _windowTitle;		//SDLInterface Title
	int _screenWidth;				//Screen width in pixels				
	int _screenHeight;				//Screen height in pixels				
	GameState _gameState;			//It describes the game state				
	SDLInterface _graphic;			//Manage the SDL graphic library		
	Network _network;
	Buffer _buffer;
	

												//Internal methods for the game execution
	void init();
	void gameLoop();
	void executePlayerCommands();
	void doPhysics();
	void renderGame();
	void networking();
	void drawMenu();
	void drawGame();
	void drawRectangle(int color, int x, int y, int width, int height);
};

