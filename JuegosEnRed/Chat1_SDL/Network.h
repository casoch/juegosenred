#pragma once
#include "UDPSocket.h"

class Network
{
private:
	UDPSocketPtr udpSocket;
	SocketAddress remoteAddress;

public:
	Network(std::string miDireccion, uint16_t miPuerto, std::string otraDireccion, uint16_t otroPuerto);
	int Receive(char mensajeRecibido[]);
	int Send(char mensajeAEnviar[]);
	~Network();
};

