#include "Chat.h"
#include "SocketUtil.h"
#include <map>
#include <cstring>


int main(int argc, char ** argv)
{
	char miIP[30];
	strcpy_s(miIP, argv[1]);
	std::string strMiIP = miIP;
	uint16_t miPuertoRead = atoi(argv[2]);

	char remotaIP[30];
	strcpy_s(remotaIP, argv[3]);
	std::string strRemotaIP = remotaIP;
	uint16_t remotoPuerto = atoi(argv[4]);
	
	SocketUtil::CargarLibreria();
	
	
	Chat chat("Ventana de chat", 800, 600, strMiIP, miPuertoRead, strRemotaIP, remotoPuerto);

	try {
		chat.run();
	}
	catch (std::exception e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
	}
	//system("pause");

	
	
	SocketUtil::DescargarLibreria();
	return 0;
}