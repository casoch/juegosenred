#pragma once
#include <string>
#include <mutex>
#include <vector>

#define MAX_MENSAJES 50


class Buffer
{
private:
	std::string aMensajes[MAX_MENSAJES];
	std::mutex mMutex;
	int numMensajes;

public:
	Buffer();
	void Mensajes(std::vector<std::string>& mensajes);
	void Encolar(char mensaje[]);
	~Buffer();
};
