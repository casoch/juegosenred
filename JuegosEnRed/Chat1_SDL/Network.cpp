#include "Network.h"
#include "SocketUtil.h"
#include "SocketAddressFactory.h"

Network::Network(std::string miDireccion, uint16_t miPuerto, std::string otraDireccion, uint16_t otroPuerto)
{
	udpSocket = SocketUtil::CreateUDPSocket();
	udpSocket->SetNonBlockingMode(true);
	char aPuerto[6];
	_itoa(miPuerto, aPuerto, 10);
	miDireccion = miDireccion + ":" + aPuerto;
	SocketAddress saDireccion = *(SocketAddressFactory::CreateIPv4FromString(miDireccion));

	char aaPuerto[6];
	_itoa(otroPuerto, aaPuerto, 10);
	otraDireccion = otraDireccion + ":" + aaPuerto;
	remoteAddress = *(SocketAddressFactory::CreateIPv4FromString(otraDireccion));

	int err = udpSocket->Bind(saDireccion);
}

int Network::Receive(char mensajeRecibido[])
{
	SocketAddress sa;
	int bytesRecibidos = udpSocket->ReceiveFrom((char*)mensajeRecibido, 100, sa);
	if (bytesRecibidos > 0)
	{
		mensajeRecibido[bytesRecibidos] = '\0';
	}
	return bytesRecibidos;
	
}

int Network::Send(char mensajeAEnviar[])
{
	int bytesEnviados = udpSocket->SendTo(mensajeAEnviar, 100, remoteAddress);
	return bytesEnviados;
}

Network::~Network()
{
}



