#include "SocketUtil.h"
#include <iostream>


UDPSocketPtr SocketUtil::CreateUDPSocket()
{
	SOCKET s = socket(AF_INET, SOCK_DGRAM, 0);
	if (s != INVALID_SOCKET)
	{
		return UDPSocketPtr(new UDPSocket(s));
	}
	else
	{
		SocketUtil::ReportError("SocketUtil::CreateUDPSocket");
		return nullptr;
	}
}

int SocketUtil::CargarLibreria()
{
	WSADATA wsa;
	int result = WSAStartup(MAKEWORD(2, 2), &wsa);
	return result;
}

int SocketUtil::DescargarLibreria()
{
	int result = WSACleanup();
	return result;
}

void SocketUtil::ReportError(std::string error)
{
	std::cout << "Error en SocketUtil: " << error << std::endl;
}

int SocketUtil::GetLastError()
{
	return 0;
}
