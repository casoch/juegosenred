#include "Chat.h"
#include "SDLInterface.h"

/**
* Constructor
* Tip: You can use an initialization list to set its attributes
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Chat::Chat(std::string windowTitle, int screenWidth, int screenHeight, std::string myAdr, uint16_t myPort, std::string remAdr, uint16_t remPort) :
	_windowTitle(windowTitle),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_gameState(GameState::INIT),
	_network(myAdr, myPort, remAdr, remPort)
	
{
	
}

/**
* Destructor
*/
Chat::~Chat()
{
}

/*
* Game execution
*/
void Chat::run() {
	//Prepare the game components
	init();
	//Start the game if everything is ready
	gameLoop();
}

/*
* Initialize all the components
*/
void Chat::init() {
	srand((unsigned int)time(NULL));
	//Create a window
	_graphic.createWindow(_windowTitle, _screenWidth, _screenHeight, false);
	_graphic.setWindowBackgroundColor(0, 0, 0, 255);
	
	//Set the font style
	
	_graphic.setFontStyle(TTF_STYLE_NORMAL);

}


/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Chat::gameLoop() {
	_gameState = GameState::PLAY;

	while (_gameState != GameState::EXIT) {
		//Detect keyboard and/or mouse events
		_graphic.detectInputEvents();
		//Execute the player commands 
		executePlayerCommands();
		
		networking();
		//Update the game physics
		doPhysics();
		//Render game
		renderGame();
	}
}


void Chat::networking()
{
	char mensaje[1300];
	int numBytes = _network.Receive(mensaje);
	if (numBytes > 0)
	{
		_buffer.Encolar(mensaje);
	}
}

/**
* Executes the actions sent by the user by means of the keyboard and mouse
* Reserved keys:
- up | left | right | down moves the player object
- m opens the menu
*/
void Chat::executePlayerCommands() {

	if (_graphic.isKeyPressed(SDLK_F1))
	{
		_graphic.setBuffering(true);
	}
	if (_graphic.isKeyPressed(SDLK_RETURN) || _graphic.isKeyPressed(SDLK_RETURN2))
	{
		std::string miMsg = _graphic.getBuffer();
		char * cstr = new char[miMsg.length() + 1];
		std::strcpy(cstr, miMsg.c_str());
		_network.Send(cstr);
		_buffer.Encolar(cstr);
		delete[] cstr;
		_graphic.setBuffering(false);
	}

	if (_graphic.isKeyPressed(SDLK_ESCAPE)) {
		_gameState = GameState::EXIT;
	}

}

/*
* Execute the game physics
*/
void Chat::doPhysics() {
	//Check if player has hit a/some monster/s
	
	//Update the animation of monsters
	
	//Update the random positions based on the MONSTER_REFRESH_FREQUENCY
	
}

/**
* Render the game on the screen
*/
void Chat::renderGame() {
	//Clear the screen
	_graphic.clearWindow();

	//Draw the screen's content based on the game state
	//if (_gameState == GameState::MENU) {
	//	drawMenu();
	//}
	//else {
	drawGame();

	//}
	//Refresh screen
	_graphic.refreshWindow();

}

/*
* Draw the game menu
*/
void Chat::drawMenu() {

}

/*
* Draw the game, winner or loser screen
*/
void Chat::drawGame() {
	
	//Pintar el juego en s�
	drawRectangle(WHITE, 0, 550, 800, 50);
	std::string texto = _graphic.getBuffer();
	_graphic.drawText(texto, BLUE, 1, 550);

	drawRectangle(BLUE, 0, 0, 800, 550);
	std::vector<std::string> aMsg;
	_buffer.Mensajes(aMsg);
	for (int i = 0; i < aMsg.size(); i++)
	{
		_graphic.drawText(aMsg[i], WHITE, 1, 10*i);
	}

}



void Chat::drawRectangle(int color, int x, int y, int width, int height)
{
	_graphic.drawFilledRectangle(color, x, y, width, height);
}
