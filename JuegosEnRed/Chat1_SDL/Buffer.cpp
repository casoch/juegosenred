#include "Buffer.h"
#include <iostream>
#include <Windows.h>



Buffer::Buffer()
{
	numMensajes = 0;
}



void Buffer::Mensajes(std::vector<std::string>& mensajes)
{
	mMutex.lock();
	std::vector<std::string> _mensajes(numMensajes);
	for (int i = 0; i < numMensajes; i++)
	{
		_mensajes[i] = aMensajes[i];
	}
	mensajes = _mensajes;
	mMutex.unlock();
}

void Buffer::Encolar(char mensaje[])
{
	std::string str = mensaje;
	mMutex.lock();
	if (numMensajes == MAX_MENSAJES)
	{
		for (int i = 1; i < MAX_MENSAJES; i++)
		{
			aMensajes[i - 1] = aMensajes[i];
		}
		aMensajes[MAX_MENSAJES - 1] = str;
	}
	else
	{
		aMensajes[numMensajes] = str;
		numMensajes++;
	}
	mMutex.unlock();
}



Buffer::~Buffer()
{
}
