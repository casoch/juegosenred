#pragma once
#include <string>
#include <SocketTools.h>
#include <UDPSocket.h>

void ServerRun(std::string _serverAddress)
{
	SocketAddress saServer;
	saServer.SetAddress(_serverAddress);
	UDPSocket serverSocket;
	serverSocket.Bind(saServer);

	char buffer[50];
	SocketAddress from;
	int bytesReceived = serverSocket.ReceiveFrom(buffer, 50, from);
	std::cout << "Me escribe la direcci�n: " << from << std::endl;
	if (bytesReceived > 0)
	{
		std::cout << "Recibo " << buffer << std::endl;
		std::string wlcm = "WELLCOME";
		serverSocket.SendTo(wlcm.c_str(), wlcm.size(), from);
		std::cout << "Env�o " << wlcm << std::endl;
	}
}

void ClientRun(std::string _serverAddress, std::string _clientAddress)
{
	SocketAddress saServer;
	saServer.SetAddress(_serverAddress);
	SocketAddress saClient;
	saClient.SetAddress(_clientAddress);

	UDPSocket clientSocket;
	clientSocket.Bind(saClient);

	std::string hello = "HELLO";
	clientSocket.SendTo(hello.c_str(), hello.size(), saServer);
	std::cout << "Env�o " << hello << std::endl;
	char buffer[50];
	SocketAddress from;
	int bytesReceived = clientSocket.ReceiveFrom(buffer, 50, from);
	std::cout << "Me escribe la direcci�n: " << from << std::endl;
	if (bytesReceived > 0)
	{
		std::cout << "Recibo " << buffer << std::endl;
	}
}

void main(int args, char* argv[])
{
	SocketTools::CargarLibreria();
	std::string function = argv[1];
	std::string serverAddress = argv[2];

	if (function == "servidor")
	{
		ServerRun(serverAddress);
	}
	else if (function == "cliente")
	{
		std::string clientAddress = argv[3];
		ClientRun(serverAddress, clientAddress);

	}
	SocketTools::DescargarLibreria();
	system("pause");
}