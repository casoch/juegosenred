#include "NetworkManager.h"


NetworkManager::NetworkManager(std::string _serverAddress, std::string _nick, UserData* _userData): nick(_nick)
{
	userData = _userData;
	SocketAddress sa;
	sa.SetAddress(_serverAddress);

	int err = tcpSocket.Connect(sa);
	if (err == -1)
	{
		throw std::exception("Error en connect");
	}

	
}


int NetworkManager::Receive(std::string & _message)
{
	char buffer[1300];
	int err = tcpSocket.Receive(buffer, 1300);
	if (err > 0 && err < 1300)
	{
		buffer[err] = '\0';
	}
	if (err > 0)
	{
		_message = buffer;
	}
	return err;
}

int NetworkManager::Send(std::string _message)
{
	int bytes = tcpSocket.Send(_message.c_str());
	return bytes;
}

void NetworkManager::ProcessMessage(std::string _message)
{
	int index_ = _message.find_first_of('_');
	std::string cabecera = _message.substr(0, index_);
	std::string contenido = _message.substr(index_ + 1, _message.size() - index_);
	
	if (cabecera == "BEGIN")
	{
		std::string strNickSend = "NICK_";
		strNickSend = strNickSend.append(nick);
		tcpSocket.Send(strNickSend.c_str());
		
		int err = tcpSocket.NonBlocking(true);
		if (err == -1)
		{
			throw std::exception("Error en nonblocking");
		}
		std::cout << "Empieza la partida." << std::endl;
	}
	else if (cabecera == "WORD")
	{
		wordToCopy = contenido;
		std::cout << "Escribe: " << wordToCopy << std::endl;
	}
	else if (cabecera == "KOWORD")
	{
		std::cout << "Te has equivocado." << std::endl;
		std::cout << "Escribe: " << wordToCopy << std::endl;
	}
	else if (cabecera == "OKWORD")
	{
		if (contenido == nick)
		{
			std::cout << "Acertaste, " << nick << std::endl;
		}
		else
		{
			std::cout << "Te gan� " << contenido << std::endl;
		}
	}
	else if (cabecera == "SCORE")
	{
		std::cout << "Ranking actual: " << contenido << std::endl;
	}
}

NetworkManager::~NetworkManager()
{

}