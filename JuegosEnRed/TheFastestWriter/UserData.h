#pragma once
#include <string>
#include <mutex>

class UserData
{
private:
	std::string word;
	bool sent;
	std::mutex mtx;

public:
	UserData()
	{
		sent = true;
	}
	void SetWord(std::string word)
	{
		mtx.lock();
		this->word = word;
		sent = false;
		mtx.unlock();
	}
	bool GetWord(std::string& word)
	{
		bool ret;
		mtx.lock();
		if (sent)
		{
			ret = false;
		}
		else
		{
			word = this->word;
			sent = true;
			ret = true;
		}
		mtx.unlock();
		return ret;
	}
	~UserData() {}
};

