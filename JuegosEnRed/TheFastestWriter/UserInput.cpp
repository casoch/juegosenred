#include "UserInput.h"

std::string UserInput::PideDatosTeclado()
{
	std::string strDatos;
	std::getline(std::cin, strDatos);
	int longitud = strDatos.length();

	
	return strDatos;
}


UserInput::UserInput(UserData * userData)
{
	this->userData = userData;
}

void UserInput::operator()()
{
	while (true)
	{
		std::string input = PideDatosTeclado();
		std::string sending = "WRITE_";
		sending.append(input);
		userData->SetWord(input);
	}
}

UserInput::~UserInput()
{
}
