#pragma once
#include <string>
#include <SocketTools.h>
#include <TCPSocket.h>
#include <exception>
#include "UserInput.h"
#include "NetworkManager.h"
#include <thread>



void Client(std::string _addressServer, std::string _nick, UserData* _userData)
{
	NetworkManager networkManager(_addressServer, _nick, _userData);
	while (true)
	{
		std::string message;
		int bytesReceived = networkManager.Receive(message);
		if (bytesReceived > 0)
		{
			networkManager.ProcessMessage(message);
		}
		else if (bytesReceived == 0)
		{
			break;
		}

		std::string word;
		bool enviar = _userData->GetWord(word);
		if (enviar)
		{
			//std::cout << "El usuario escribe " << word << std::endl;
			std::string strWordToSend = "WRITE_";
			strWordToSend = strWordToSend.append(word);
			networkManager.Send(strWordToSend.c_str());
		}
	}
	std::cout << "Salgo del while true" << std::endl;
}

void main(int args, char* argv[])
{
	std::string addressServer = argv[1];
	std::string nick = argv[2];

	SocketTools::CargarLibreria();
	UserData userData;
	UserInput userInput(&userData);
	std::thread tInput(userInput);
	tInput.detach();
	
	Client(addressServer, nick, &userData);
	SocketTools::DescargarLibreria();
	exit(0);
}
