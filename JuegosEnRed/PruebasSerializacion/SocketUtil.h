#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include "UDPSocket.h"


enum SocketAddressFamily { INET = AF_INET, INET6 = AF_INET6 };


class SocketUtil
{

public:

	static UDPSocketPtr CreateUDPSocket();

	static int CargarLibreria();

	static int DescargarLibreria();

	static void ReportError(std::string error);

	static int GetLastError();
};
