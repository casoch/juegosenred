#include "OutputMemoryStream.h"
#include <algorithm>


OutputMemoryStream::OutputMemoryStream():mBuffer(nullptr), mHead(0), mCapacity(0)
{
	ReallocBuffer(32);
}


OutputMemoryStream::~OutputMemoryStream()
{
	delete[] mBuffer;
}

const char * OutputMemoryStream::GetBufferPtr() const
{
	return mBuffer;
}

uint32_t OutputMemoryStream::GetLength() const
{
	return mHead;
}

void OutputMemoryStream::Write(const void * inData, size_t inByteCount)
{
	//Nos aseguramos de que tenemos suficiente espacio en el buffer
	uint32_t resultHead = mHead + static_cast<uint32_t>(inByteCount);
	if (resultHead > mCapacity)
	{
		ReallocBuffer(std::max(mCapacity * 2, resultHead));
	}

	//Copiamos los datos en la primera posici�n libre del buffer
	memcpy(mBuffer + mHead, inData, inByteCount);

	//dejamos en mHead en la posici�n correcta para el siguiente write
	mHead = resultHead;
}

void OutputMemoryStream::Write(uint32_t inData)
{
	Write(&inData, sizeof(inData));
}

void OutputMemoryStream::ReallocBuffer(uint32_t inNewLength)
{
}
