#include "UDPSocket.h"
#include "SocketUtil.h"



int UDPSocket::Bind(const SocketAddress& inToAddress)
{
	uint32_t size = inToAddress.GetSize();
	int err = bind(mSocket, &(inToAddress.mSockAddr), size);
	int lasterror = WSAGetLastError();
	if (err != 0)
	{

		SocketUtil::ReportError("UDPSocket::Bind");
		return -SocketUtil::GetLastError();
	}
	return NO_ERROR;
}

int UDPSocket::SendTo(const void* inData, int inLen, const SocketAddress & inTo)
{
	int byteSentCount = sendto(mSocket, (const char*)inData, inLen, 0, &inTo.mSockAddr, inTo.GetSize());
	if (byteSentCount >= 0)
	{
		return byteSentCount;

	}
	else
	{
		SocketUtil::ReportError("UDPSocket::SendTo");
		return -SocketUtil::GetLastError();
	}
}

int UDPSocket::ReceiveFrom(void * inBuffer, int inLen, SocketAddress & outFrom)
{
	int fromLen = outFrom.GetSize();
	int readByteCount = recvfrom(mSocket, (char*)inBuffer, inLen, 0, &outFrom.mSockAddr, &fromLen);
	int longBuffer = strlen((char*)inBuffer);
	if (readByteCount >= 0)
	{
		return readByteCount;
	}
	else
	{
		SocketUtil::ReportError("UDPSocket::ReceiveFrom");
		return -SocketUtil::GetLastError();
	}
}

UDPSocket::~UDPSocket()
{
	closesocket(mSocket);
}
