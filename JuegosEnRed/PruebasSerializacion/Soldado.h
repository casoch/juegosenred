#pragma once
#include <vector>

class Soldado
{
private:
	int numVidas;
	int numBalas;
	Soldado* aliados;
	char nombre[128];
	std::vector<int> enemigosIndices;

public:
	Soldado();
	virtual void Update();
	//void Write(OutputMemoryStream& inStream) const;
	//void Read(InputMemoryStream& inStream);
	~Soldado();
};

