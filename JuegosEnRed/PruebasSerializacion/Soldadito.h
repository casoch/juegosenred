#pragma once
#include "UDPSocket.h"

class Soldadito
{
private:
	int numVidas;
	int numBalas;
public:
	Soldadito();
	void EnviarSoldaditoSimple(UDPSocket socket, SocketAddress& to);
	void RecibirSoldaditoSimple(UDPSocket socket, SocketAddress & from);
	friend std::ostream& operator<<(std::ostream& os, Soldadito& soldadito);
	friend std::istream& operator>>(std::istream& is, Soldadito& soldadito);

	~Soldadito();
};

