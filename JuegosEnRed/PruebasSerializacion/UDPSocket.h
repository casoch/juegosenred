#pragma once
#include "SocketAddress.h"


typedef std::shared_ptr<UDPSocket> UDPSocketPtr;



class UDPSocket
{

private:
	SOCKET mSocket;
	friend class SocketUtil;
	
public:
	UDPSocket(SOCKET inSocket) :mSocket(inSocket) {

	}
	int Bind(const SocketAddress& inToAddress);
	int SendTo(const void* inData, int inLen, const SocketAddress& inTo);
	int ReceiveFrom(void* inBuffer, int inLen, SocketAddress& outFrom);
	~UDPSocket();

};
