#pragma once
#include <memory>
class OutputMemoryStream
{
private:
	char* mBuffer;
	uint32_t mHead;
	uint32_t mCapacity;
public:
	OutputMemoryStream();
	~OutputMemoryStream();

	//get a pointer to the data
	const char* GetBufferPtr() const;
	uint32_t GetLength() const;
	void Write(const void* inData, size_t inByteCount);
	void Write(uint32_t inData);
	void ReallocBuffer(uint32_t inNewLength);
};

