#include "Soldadito.h"
#include <iostream>


Soldadito::Soldadito(): numVidas(10), numBalas(100)
{

}

void Soldadito::EnviarSoldaditoSimple(UDPSocket socket, SocketAddress& to)
{
	const char* serializedData = reinterpret_cast<const char*>(this);
	socket.SendTo(serializedData, sizeof(Soldadito), to);
}

void Soldadito::RecibirSoldaditoSimple(UDPSocket socket, SocketAddress & from)
{
	socket.ReceiveFrom(reinterpret_cast<char*>(this), sizeof(Soldadito), from);
}


Soldadito::~Soldadito()
{
}

std::ostream & operator<<(std::ostream & os, Soldadito & soldadito)
{
	os << "Vidas:" << soldadito.numVidas << "\nBalas:" << soldadito.numBalas << "\n";
	return os;
}

std::istream & operator>>(std::istream & is, Soldadito & soldadito)
{
	std::cout << "Vidas: ";
	std::cin>>soldadito.numVidas;
	std::cout << "Balas: ";
	std::cin >> soldadito.numBalas;
	return is;
}
