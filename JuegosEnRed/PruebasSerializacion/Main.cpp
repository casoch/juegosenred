#include "SocketUtil.h"
#include <iostream>
#include "Soldadito.h"

void Servidor(uint16_t puerto)
{
	UDPSocketPtr udpSocket = SocketUtil::CreateUDPSocket();
	SocketAddress myAddress(L"127.0.0.1", puerto);
	
	udpSocket->Bind(myAddress);

	while (true)
	{
		SocketAddress quienEnvia;
		Soldadito soldaditoRecibido;
		udpSocket->ReceiveFrom(reinterpret_cast<char*>(&soldaditoRecibido), sizeof(Soldadito), quienEnvia);
		
		//soldaditoRecibido.RecibirSoldaditoSimple(*udpSocket, quienEnvia);
		std::cout << soldaditoRecibido << std::endl;
	}
}

void Cliente(uint16_t puerto)
{
	UDPSocketPtr udpSocket = SocketUtil::CreateUDPSocket();
	SocketAddress toAddress(L"127.0.0.1", puerto);

	while (true)
	{
		Soldadito soldadito;
		std::cin >> soldadito;
		udpSocket->SendTo(reinterpret_cast<const char*>(&soldadito), sizeof(Soldadito), toAddress);
	}
}

int main(int argc, char** argv)
{
	SocketUtil::CargarLibreria();
	char funcion[100];
	strcpy_s(funcion, argv[1]);

	if (strcmp(funcion, "servidor") == 0)
	{
		uint16_t puertoEscuchar = atoi(argv[2]);
		Servidor(puertoEscuchar);
	}
	else if (strcmp(funcion, "cliente") == 0)
	{
		std::string direccionDestino = argv[2];
		uint16_t puertoDestino = atoi(argv[3]);
		Cliente(puertoDestino);

	}
	else
	{
		std::cout << "Funcion " << funcion << " no permitida." << std::endl;
	}
	SocketUtil::DescargarLibreria();
	return 0;
}