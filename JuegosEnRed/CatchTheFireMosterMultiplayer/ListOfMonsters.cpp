#include "ListOfMonsters.h"


/*
* Constructor
* @param n is the number of monsters to store
*/
ListOfMonsters::ListOfMonsters() {
		
}

/*
* Destructor
*/
ListOfMonsters::~ListOfMonsters(){
	
}

int ListOfMonsters::size()
{
	return monsters.size();
}

/*
* Get a specified monster.This method is equivalent to getMonster();
* @param id identifies the monster
* @return a reference to the idTH monster, so, if user modifies it, the information of the data structure will be also modified
*/
Sprite& ListOfMonsters::operator[](int id) {
	if ((id < 0) && (id >= monsters.size())) throw std::exception("Invalid access to the data structure, this id doesn't exist");
	return monsters[id];
}

void ListOfMonsters::Deserialize(std::string strMonsters)
{
	monsters.clear();
	int index_ = strMonsters.find_first_of('_');
	std::string cabecera = strMonsters.substr(0, index_);
	std::string contenido = strMonsters.substr(index_ + 1, strMonsters.size() - index_);
	
	int indexAlm = contenido.find_first_of('#');
	std::string strNumMonsters = contenido.substr(0, indexAlm);
	int numMonsters = atoi(strNumMonsters.c_str());

	for (size_t i = 0; i < numMonsters; i++)
	{
		contenido = contenido.substr(indexAlm + 1, contenido.size() - indexAlm);
		indexAlm = contenido.find_first_of('#');
		std::string strPositionMonster = contenido.substr(0, indexAlm);

		int index2Ptos = strPositionMonster.find_first_of(':');
		std::string strX = strPositionMonster.substr(0, index2Ptos);
		std::string strY = strPositionMonster.substr(index2Ptos + 1, strPositionMonster.size() - index2Ptos);
		int x = atoi(strX.c_str());
		int y = atoi(strY.c_str());

		Sprite s;
		s.setInitialValues(0, 0, SPRITE_FIRE, 0, 5);
		s.setPositionAtWorld(x, y);
		monsters.push_back(s);
	}

	
}

/*
* Update the frame of all monsters
* @param curTicks is the current tick
*/
void ListOfMonsters::nextAnimationFrame(int curTicks) {
	for (int i = 0; i < monsters.size(); i++) {
		monsters[i].nextFrame((int)(curTicks * SPRITE_SPEED));
	}
}

