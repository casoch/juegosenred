#include "Game.h"
#include <SocketTools.h>
#include <UDPSocket.h>

//void Cliente()
//{
//	SocketAddress saServer;
//	SocketAddress myAddress;
//	saServer.SetAddress("localhost:5000");
//	myAddress.SetAddress("localhost:5001");
//
//	UDPSocket udp;
//	udp.Bind(myAddress);
//	udp.NonBlocking(true);
//	char buffer[1300];
//	std::string mensaje = "Soy cliente";
//	SocketAddress from;
//
//
//	while (true)
//	{
//		int bytesEnviados = udp.SendTo(mensaje.c_str(), mensaje.size(), saServer);
//		std::cout << "Bytes enviados: " << bytesEnviados << std::endl;
//		int bytesRecibidos = udp.ReceiveFrom(buffer, 1300, from);
//		std::cout << "Bytes recibidos: " << bytesRecibidos << std::endl;
//	}
//}

void Launch(std::string serverAddress, std::string clientAddress, std::string nick)
{
	Game game("Catch the fire monsters", SCREEN_WIDTH, SCREEN_HEIGHT, serverAddress, clientAddress, nick);

	try {
		game.run();
	}
	catch (std::exception e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
	}
}

int main(int argc, char ** argv) {
	SocketTools::CargarLibreria();
	std::string serverAddress = argv[1];
	std::string clientAddress = argv[2];
	std::string nick = argv[3];
	
	Launch(serverAddress, clientAddress, nick);

	SocketTools::DescargarLibreria();
	return 0;
}