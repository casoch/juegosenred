#pragma once
#include <UDPSocket.h>
#include <GameConstants.h>
#include "GameConstantsClient.h"
#include "ListOfMonsters.h"
#include <time.h>

class Network
{
private:
	UDPSocket udpSocket;
	SocketAddress saServer;
	std::string nick;
	NetworkClientState networkState;
	clock_t mTimeOfLastHello;
	
public:
	Network(std::string addressServer, std::string addressClient, std::string nick);
	void Send(std::string mensaje);
	std::string Receive();
	void NonBlocking(bool ok);
	NetworkClientState GetState();
	void SetState(NetworkClientState _state);
	
	void SayHello();
	void ProcessWelcome();
	void ProcessMonsters(ListOfMonsters& listOfMonsters, std::string strMonsters);
	//void ProcessScore();
	
	~Network();
};

