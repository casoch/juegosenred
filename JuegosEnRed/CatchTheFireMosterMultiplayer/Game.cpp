#include "Game.h"

/**
* Constructor
* Tip: You can use an initialization list to set its attributes
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, std::string serverAddress, std::string clientAddress, std::string nick) :
	_windowTitle(windowTitle),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_gameState(GameState::INIT),
	_network(serverAddress, clientAddress, nick)
{
}

/**
* Destructor
*/
Game::~Game()
{
	
}

/*
* Game execution
*/
void Game::run() {
		//Prepare the game components
	init();
		//Start the game if everything is ready
	gameLoop();
}

/*
* Initialize all the components
*/
void Game::init() {
	srand((unsigned int)time(NULL));
		//Create a window
	_graphic.createWindow(_windowTitle, _screenWidth, _screenHeight, false);	
	_graphic.setWindowBackgroundColor(255, 255, 255, 255);
		//Load the sprites associated to the different game elements
	_graphic.loadTexture(SPRITE_FIRE, "../sharedResources/images/characters/fireSprite.png");
		//Set the font style
	_graphic.setFontStyle(TTF_STYLE_NORMAL);
		//Initialize the game elements
			//The different monsters
}


/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {	
	while (_gameState == GameState::INIT)
	{
		_network.SayHello();
		Receiving();
		renderGame();
	}
	while (_gameState == GameState::PLAY) {		
		Receiving();
		//Update the game physics
		doPhysics();
		//Detect keyboard and/or mouse events
		_graphic.detectInputEvents();
		//Execute the player commands 
		executePlayerCommands();
		//Render game
		renderGame();			
	}
}

void Game::Receiving()
{
	std::string strReceived = _network.Receive();
	int index_ = strReceived.find_first_of('_');
	std::string cabecera = strReceived.substr(0, index_);
	if (cabecera == "WELCOME" && _gameState == GameState::INIT)
	{
		_network.SetState(NetworkClientState::NCS_Welcomed);
	}
	else if (cabecera == "BEGIN" && _gameState == GameState::INIT)
	{
		std::cout << "Inicio el clock" << std::endl;
		_initTimePlaying = clock();
	}
	else if (cabecera == "END")
	{
		_gameState = GameState::EXIT;
	}
	else if (cabecera == "MONSTERS")
	{
		_gameState = GameState::PLAY;
		_monsters.Deserialize(strReceived);
	}
}

/**
* Executes the actions sent by the user by means of the keyboard and mouse
* Reserved keys:
- up | left | right | down moves the player object
- m opens the menu
*/
void Game::executePlayerCommands() {

	if (_graphic.isKeyPressed(SDL_BUTTON_LEFT) && _monsters.size() > 0){
		glm::ivec2 mouseCoords = _graphic.getMouseCoords();
		std::string strPosicion = "POSITION_";
		strPosicion = strPosicion.append(std::to_string(mouseCoords.x));
		strPosicion = strPosicion.append(":");
		strPosicion = strPosicion.append(std::to_string(mouseCoords.y));
		std::cout << "Env�o: " << strPosicion << std::endl;
		_network.Send(strPosicion);
	}

	if (_graphic.isKeyPressed(SDLK_ESCAPE)) {
		_gameState = GameState::EXIT;
	}
}

/*
* Execute the game physics
*/
void Game::doPhysics() {
	

	if (_monsters.size()!=0)	{
			//Update the animation of monsters
		_monsters.nextAnimationFrame(_graphic.getCurrentTicks());
		
	}	
}

/**
* Render the game on the screen
*/
void Game::renderGame() {
	//Clear the screen
	_graphic.clearWindow();

		//Draw the screen's content based on the game state
	if (_gameState == GameState::INIT) 
	{
		drawInitInfo();
	}
	else if (_gameState == GameState::PLAY) 
	{
		drawGame();
	}
		//Refresh screen
	_graphic.refreshWindow();
}

/*
* Draw the game menu
*/
void Game::drawInitInfo() 
{
	NetworkClientState state = _network.GetState();

	switch (state)
	{
	case NetworkClientState::NCS_Uninitialized:
		_graphic.drawText("Iniciando", WHITE, RED, 0, 0);
		break;
	case NetworkClientState::NCS_SayingHello:
		_graphic.drawText("Conectando con servidor", WHITE, RED, 0, 0);
		break;
	case NetworkClientState::NCS_Welcomed:
		_graphic.drawText("Esperando jugadores", WHITE, RED, 0, 0);
		break;
	default:
		break;
	}
	
}

/*
* Draw the game menu
*/
void Game::drawMenu() {

}

/*
* Draw the game, winner or loser screen
*/
void Game::drawGame()	{
		//Check if game is over
	if (_monsters.size() == 0 && _gameState == GameState::PLAY)
	{		
			//Draw the score
		_graphic.drawText("WELL DONE!", WHITE, BLACK, _screenWidth / 2 - _screenWidth / 14, _screenHeight / 2 - _screenHeight / 6);		
		//_graphic.drawText("Your score: " + std::to_string(_lastTimeMonsterWasUpdated / 1000) + " seconds", WHITE, BLACK, _screenWidth / 2 - _screenWidth / 8, _screenHeight / 2);
		_graphic.drawText("Press ESC to exit", WHITE, BLUE, _screenWidth / 2 - _screenWidth / 10, _screenHeight / 2 + _screenHeight / 4);

	}else if (_network.GetState()==NetworkClientState::NCS_Welcomed && _gameState == GameState::PLAY){
			//Draw the list of monsters
		for (int i = 0; i < _monsters.size(); i++) {
			drawSprite(_monsters[i]);
		}
			//Draw the current time
		
		_graphic.drawText("Current time: " + std::to_string((clock() - _initTimePlaying) / 1000) + " seconds", BLACK, WHITE, 0, 0);
	}
}

/*
* Draw the sprite on the screen
* @param sprite is the sprite to be displayed
*/
void Game::drawSprite(Sprite & sprite) {
	_graphic.drawTexture(sprite.getSpriteId(), MONSTER_DEFAULT_WIDTH*sprite.getCurrentFrame(), 0, MONSTER_DEFAULT_WIDTH, MONSTER_DEFAULT_HEIGHT
		, sprite.getXAtWorld(), sprite.getYAtWorld(), MONSTER_DEFAULT_WIDTH, MONSTER_DEFAULT_HEIGHT);
}