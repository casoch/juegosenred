#include "Network.h"



Network::Network(std::string addressServer, std::string addressClient, std::string _nick): nick(_nick), networkState(NetworkClientState::NCS_Uninitialized)
{
	saServer.SetAddress(addressServer);
	SocketAddress myAddress;
	myAddress.SetAddress(addressClient);
	udpSocket.Bind(myAddress);
	NonBlocking(true);
}

void Network::Send(std::string mensaje)
{
	int bytes = udpSocket.SendTo(mensaje.c_str(), mensaje.size(), saServer);
	std::cout << "Bytes enviados: " << bytes << std::endl;
}

std::string Network::Receive()
{
	std::string ret="";
	char buffer[MAX_BUFFER];
	SocketAddress from;
	int bytesReceived = udpSocket.ReceiveFrom(buffer, MAX_BUFFER, from);
	//std::cout << "Bytes recibidos: " << bytesReceived << std::endl;
	if (bytesReceived > 0)
	{
		if (bytesReceived < MAX_BUFFER)
		{
			buffer[bytesReceived] = '\0';
		}
		ret = buffer;
	}
	return ret;
}

void Network::NonBlocking(bool ok)
{
	udpSocket.NonBlocking(ok);
}



NetworkClientState Network::GetState()
{
	return networkState;
}

void Network::SetState(NetworkClientState _state)
{
	networkState = _state;
}

void Network::SayHello()
{
	if (networkState == NetworkClientState::NCS_Welcomed)
	{
		return;
	}
	if (networkState == NetworkClientState::NCS_Uninitialized)
	{
		networkState = NetworkClientState::NCS_SayingHello;
	}
	
	clock_t time = clock();
	if (time > mTimeOfLastHello + FREQUENCY_SAYING_HELLO)
	{
		networkState = NetworkClientState::NCS_SayingHello;
		std::string message = HEADER_HELLO;
		message = message.append(nick);
		Send(message);
		mTimeOfLastHello = time;
	}
}

void Network::ProcessWelcome()
{
	networkState = NetworkClientState::NCS_Welcomed;
}

void Network::ProcessMonsters(ListOfMonsters & listOfMonsters, std::string strMonsters)
{
	listOfMonsters.Deserialize(strMonsters);
}


Network::~Network()
{
	
}
