#pragma once
#include <TCPSocket.h>
#include <exception>
#include "ClientProxy.h"
#include <SocketTools.h>


#define STATE_PLAYING 1
#define STATE_EXIT 2

ClientProxy aClientProxy[NUM_PLAYERS];
WorldMap worldMap;
int gameState;


void ProcessClientCommand(int idPlayer, std::string cmd)
{
	if (cmd == "FIN")
	{
		gameState = STATE_EXIT;
	}
	else
	{
		int index_ = cmd.find_first_of('_', 0);
		if (index_ == -1)
		{
			return;
		}
		else
		{
			std::string typeCmd = cmd.substr(0, index_);
			if (typeCmd == "NAME")
			{
				//Me env�a su nombre
				std::string name = cmd.substr(index_, cmd.size() - index_);
				worldMap.SetNamePlayer(idPlayer, name);
			}
			else if (typeCmd == "GO")
			{
				//Avanza 1 casilla
				worldMap.MovePlayer(idPlayer);
			}
			else if (typeCmd == "BOMB")
			{
				//Pone una bomba en su posici�n
				worldMap.ShootBomb(idPlayer);
			}
			else if (typeCmd == "ROTATE")
			{
				//Rota al player
				std::string direction = cmd.substr(index_, cmd.size() - index_);
				worldMap.RotatePlayer(idPlayer, direction == "LEFT");
			}
		}
	}
}

int main(int args, char* argv[])
{
	SocketTools::CargarLibreria();
	gameState = STATE_PLAYING;
	std::string strAddress = argv[1];
	TCPSocket tcpSocket;
	SocketAddress sa;
	sa.SetAddress(strAddress);
	int err = tcpSocket.Bind(sa);
	if (err == -1)
	{
		throw std::exception("Error en main::Bind");
	}
	err = tcpSocket.Listen(4);
	if (err == -1)
	{
		throw std::exception("Error en main::Listen");
	}
	SocketAddress from;
	for (size_t i = 0; i < NUM_PLAYERS; i++)
	{
		std::shared_ptr<TCPSocket> tcpClient = tcpSocket.Accept(from);
		tcpClient->NonBlocking(true);
		aClientProxy[i].SetTCPSocket(tcpClient);
	}
	worldMap.LoadFromFile("Map.txt");

	do
	{
		for (size_t i = 0; i < NUM_PLAYERS; i++)
		{
			aClientProxy[i].SendWorldToClient(worldMap);
		}
		//Miro si los jugadores quieren comunicar algo
		for (size_t i = 0; i < NUM_PLAYERS; i++)
		{
			std::string cmd = aClientProxy[i].ReceiveCommand();
			ProcessClientCommand(i, cmd);
			if (gameState == STATE_EXIT)
			{
				break;
			}
		}
	} while (true);
	SocketTools::DescargarLibreria();
	return 0;
}