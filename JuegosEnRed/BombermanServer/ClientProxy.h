#pragma once
#include <TCPSocket.h>
#include <memory>
#include <Player.h>
#include <WorldMap.h>

class ClientProxy
{
private:
	std::shared_ptr<TCPSocket> tcpSocketPtr;

public:
	ClientProxy();
	void SetTCPSocket(std::shared_ptr<TCPSocket> tcpSocketPtr);
	void SendWorldToClient(WorldMap& worldMap);
	std::string ReceiveCommand();
	~ClientProxy();
};
