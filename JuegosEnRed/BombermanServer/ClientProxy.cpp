#pragma once
#include "ClientProxy.h"


ClientProxy::ClientProxy()
{
	
}

void ClientProxy::SetTCPSocket(std::shared_ptr<TCPSocket> tcpSocketPtr)
{
	this->tcpSocketPtr = tcpSocketPtr;
}

void ClientProxy::SendWorldToClient(WorldMap & worldMap)
{
	std::string strWorldMap2D = worldMap.SerializeWorldMap2D();
	std::string strPlayers = worldMap.SerializePlayers();
	std::string strBombs = worldMap.SerializeBombs();
	std::string strBonus = worldMap.SerializeBonus();

	tcpSocketPtr->Send(strWorldMap2D.c_str());
	tcpSocketPtr->Send(strPlayers.c_str());
	tcpSocketPtr->Send(strBombs.c_str());
	tcpSocketPtr->Send(strBonus.c_str());
}

std::string ClientProxy::ReceiveCommand()
{
	char buffer[1300];
	int bytesReceived = tcpSocketPtr->Receive(buffer, 1300);
	if (bytesReceived == 0)
	{
		return "FIN";
	}
	else if (bytesReceived > 0)
	{
		if (bytesReceived < 1300)
		{
			buffer[bytesReceived] = '\0';
		}
		return buffer;
	}
	return "";
}

ClientProxy::~ClientProxy()
{

}
