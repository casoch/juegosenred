#pragma once
class Enemigo
{
private:
	int x, y;
	
public:
	Enemigo(int x, int y);
	Enemigo(const Enemigo& enemigo);
	bool operator==(Enemigo& enemigo);
	int GetX();
	int GetY();
	~Enemigo();
};

