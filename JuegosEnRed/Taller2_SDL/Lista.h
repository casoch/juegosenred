#pragma once
#include <vector>
#include "Enemigo.h"

template<class T>
class Lista:public std::vector<T>
{
public:
	Lista() {}

	void Add(T item)
	{
		push_back(item);
	}
	void Remove(T item)
	{
		for (size_t i = 0; i < size(); i++)
		{
			if (at(i) == item)
			{
				Remove(i);
				break;
			}
		}
	}
	void Remove(int i)
	{
		erase(begin() + i);
	}
	bool Empty()
	{
		return size() == 0;
	}
	T Search(T item)
	{
		for (size_t i = 0; i < size(); i++)
		{
			if (at(i) == item)
			{
				return (i);
			}
		}
	}
	
	~Lista() {}
};

