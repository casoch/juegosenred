#include "Game.h"
#include "SDLInterface.h"
#include <string>

/**
* Constructor
* Tip: You can use an initialization list to set its attributes
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, std::string direccionDestino) :
	_windowTitle(windowTitle),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_gameState(GameState::INIT),
	_disparo(0,0), _lastDisparo(0,0)
{
	SocketAddress sa;
	sa.SetAddress(direccionDestino);
	int err = tcpSocket.Connect(sa);
	if (err > -1)
	{
		err = tcpSocket.NonBlocking(true);
		std::cout << "Conexi�n establecida correctamente." << std::endl;
		std::cout << "Non blocking: " << err << std::endl;
	}
}

/**
* Destructor
*/
Game::~Game()
{
}

/*
* Game execution
*/
void Game::run() {
	//Prepare the game components
	init();
	//Start the game if everything is ready
	gameLoop();
}

/*
* Initialize all the components
*/
void Game::init() {
	_graphic.createWindow(_windowTitle, _screenWidth, _screenHeight, false);
	_graphic.setFontStyle(TTF_STYLE_NORMAL);
}


/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {
	_gameState = GameState::PLAY;
	bool hayUpdates = true;
	while (_gameState != GameState::EXIT) {
		//Detect keyboard and/or mouse events
		
		_graphic.detectInputEvents();
		executePlayerCommands();
		SendInputEvents();
		hayUpdates = ReceiveUpdates();
		if (hayUpdates)
		{
			renderGame();
		}

	}
}

void Game::SendInputEvents()
{
	if (_disparo.x != _lastDisparo.x || _disparo.y != _lastDisparo.y)
	{
		_lastDisparo.x = _disparo.x;
		_lastDisparo.y = _disparo.y;
		int posX = (_disparo.x * 32)/800;
		int posY = (_disparo.y * 20)/500;
		std::string strSend = std::to_string(posX);
		strSend = strSend.append(":");
		strSend = strSend.append(std::to_string(posY));
		int err = tcpSocket.Send(strSend.c_str());
		if (err > -1)
		{
			std::cout << "Se han enviado " << err << " bytes." << std::endl;
		}
	}
}

bool Game::ReceiveUpdates()
{
	char data[1300];
	int err = tcpSocket.Receive(data, 1300);
	if (err > -1)
	{
		std::cout << "Se reciben " << err << " bytes." << std::endl;
		if (err<1300)
		{
			data[err] = '\0';
		}
		std::string strData = data;
		mapaEnemigos.RellenaListaEnemigos(data);
		return true;
	}
	return false;
}

/**
* Executes the actions sent by the user by means of the keyboard and mouse
* Reserved keys:
- up | left | right | down moves the player object
- m opens the menu
*/
void Game::executePlayerCommands() {

	glm::ivec2 coordMouse = _graphic.getMouseCoords();
	_disparo = coordMouse;
	if (coordMouse.x != 0 || coordMouse.y != 0)
	{
		//std::cout << "(" << coordMouse.x << ", " << coordMouse.y << ")" << std::endl;
		//_disparo = coordMouse;
	}
	if (_graphic.isKeyPressed(SDLK_ESCAPE)) {
		_gameState = GameState::EXIT;
	}

}



/*
* Execute the game physics
*/
void Game::doPhysics() {
	//Check if player has hit a/some monster/s

	//Update the animation of monsters

	//Update the random positions based on the MONSTER_REFRESH_FREQUENCY

}

/**
* Render the game on the screen
*/
void Game::renderGame() {
	//Clear the screen
	_graphic.clearWindow();

	//Draw the screen's content based on the game state
	//if (_gameState == GameState::MENU) {
	//	drawMenu();
	//}
	//else {
	drawGame();

	//}
	//Refresh screen
	_graphic.refreshWindow();

}

/*
* Draw the game menu
*/
void Game::drawMenu() {

}

/*
* Draw the game, winner or loser screen
*/
void Game::drawGame() {

	//Pintar el juego en s�
	
	//Cuadradito de chat
	drawRectangle(BLACK, 0, 500, 800, 100);
	
	//Panel de cuadraditos
	drawRectangle(WHITE, 0, 0, 800, 500);
	int size = mapaEnemigos.size();
	if (size == 0)
	{
		_gameState = GameState::EXIT;
	}
	for (size_t i = 0; i < size; i++)
	{
		Enemigo e = mapaEnemigos[i];
		_graphic.drawFilledRectangle(RED, e.GetX()*25, e.GetY()*25, 25, 25);
	}
	for (size_t i = 1; i < 32; i++)
	{
		_graphic.drawLine(GREEN, i * 25, 0, i * 25, 500);
	}
	
	for (size_t i = 1; i < 20; i++)
	{
		_graphic.drawLine(GREEN, 0, i*25, 800, i*25);
	}

}



void Game::drawRectangle(int color, int x, int y, int width, int height)
{
	_graphic.drawFilledRectangle(color, x, y, width, height);
}

