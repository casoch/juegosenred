#include "MapaEnemigos.h"
#include <ctime>
#include <string>

#include <iostream>


MapaEnemigos::MapaEnemigos()
{
	
}

std::string MapaEnemigos::GeneraMapaEnemigosFromScratch()
{
	std::string mapaEnemigos = "";
	std::srand(std::time(0)); // use current time as seed for random generator
	for (int i = 0; i < 10; i++)
	{
		int randomVariable = std::rand();
		int posX = randomVariable % 32;
		randomVariable = std::rand();
		int posY = randomVariable % 20;

		mapaEnemigos = mapaEnemigos.append(std::to_string(posX));
		mapaEnemigos = mapaEnemigos.append(":");
		mapaEnemigos = mapaEnemigos.append(std::to_string(posY));

		//std::cout <<mapaEnemigos << std::endl;
		if (i < 9)
		{
			mapaEnemigos += ";";
		}
	}
	std::cout << "Se genera: " << mapaEnemigos << std::endl;
	return mapaEnemigos;
}

std::string MapaEnemigos::GeneraMapaEnemigos()
{
	std::string mapaEnemigos = "";

	for (int i = 0; i < lista.size(); i++)
	{
		Enemigo e = lista[i];
		int posX = e.GetX();
		int posY = e.GetY();

		mapaEnemigos = mapaEnemigos.append(std::to_string(posX));
		mapaEnemigos = mapaEnemigos.append(":");
		mapaEnemigos = mapaEnemigos.append(std::to_string(posY));

		if (i < lista.size()-1)
		{
			mapaEnemigos += ";";
		}
	}
	std::cout << "Se genera: " << mapaEnemigos << std::endl;
	return mapaEnemigos;
}

void MapaEnemigos::RellenaListaEnemigos(std::string mapaEnemigos)
{
	lista.clear();
	if (mapaEnemigos.length() == 0)
	{
		return;
	}
	int nextEnemigo = 0;
	while (true)
	{
		int nextPuntoComa = mapaEnemigos.find_first_of(";", nextEnemigo);
		std::string posicion = mapaEnemigos.substr(nextEnemigo, nextPuntoComa - nextEnemigo);
		nextEnemigo = nextPuntoComa + 1;

		int pos2Puntos = posicion.find_first_of(":", 0);
		int posX = atoi(posicion.substr(0, pos2Puntos).c_str());
		int posY = atoi(posicion.substr(pos2Puntos + 1, posicion.length() - pos2Puntos).c_str());
		Enemigo e(posX, posY);
		lista.Add(e);
		if (nextPuntoComa == std::string::npos)
		{
			break;
		}
	}

}

void MapaEnemigos::Remove(int posX, int posY)
{
	Enemigo e(posX, posY);
	lista.Remove(e);
}

int MapaEnemigos::size()
{
	return lista.size();
}

Enemigo MapaEnemigos::operator[](int pos)
{
	return lista[pos];
}

MapaEnemigos::~MapaEnemigos()
{
}
