#pragma once
#include "Lista.h"
#include "Enemigo.h"

class MapaEnemigos
{
private:
	Lista<Enemigo> lista;

public:
	MapaEnemigos();
	static std::string GeneraMapaEnemigosFromScratch();
	std::string GeneraMapaEnemigos();
	void RellenaListaEnemigos(std::string mapaEnemigos);
	void Remove(int posX, int posY);
	int size();
	Enemigo operator[](int pos);
	~MapaEnemigos();
};

