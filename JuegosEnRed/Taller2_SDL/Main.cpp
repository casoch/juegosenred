#include "Game.h"
#include <SocketTools.h>
#include <TCPSocket.h>
#include <map>
#include <cstring>
#include <string>
#include <ctime>
#include "MapaEnemigos.h"
#include "Enemigo.h"

void ProcesarDatos(char datos[], MapaEnemigos& mapaEnemigos)
{
	std::string strDatos = datos;
	int pos2Puntos = strDatos.find_first_of(":", 0);
	int posX = atoi(strDatos.substr(0, pos2Puntos).c_str());
	int posY = atoi(strDatos.substr(pos2Puntos + 1, strDatos.length() - pos2Puntos).c_str());
	mapaEnemigos.Remove(posX, posY);
}

void Servidor(std::string direccionRecepcion)
{
	SocketAddress miSA;
	miSA.SetAddress(direccionRecepcion);
	TCPSocket tcpSocket;
	tcpSocket.Bind(miSA);
	tcpSocket.Listen(10);
	SocketAddress saJ1, saJ2;
	
	std::shared_ptr<TCPSocket> tcpSocketJ1 = tcpSocket.Accept(saJ1);
	std::shared_ptr<TCPSocket> tcpSocketJ2 = tcpSocket.Accept(saJ2);
	tcpSocketJ1->NonBlocking(true);
	tcpSocketJ2->NonBlocking(true);

	for (int i = 0; i < 1; i++)
	{
		std::string strMapaEnemigos = MapaEnemigos::GeneraMapaEnemigosFromScratch();
		const char* chMapa = strMapaEnemigos.c_str();
		tcpSocketJ1->Send(chMapa);
		tcpSocketJ2->Send(chMapa);
		
		MapaEnemigos mapaEnemigos;
		mapaEnemigos.RellenaListaEnemigos(strMapaEnemigos);
		
		while (true)
		{
			char aDatosJ1[1300];
			int lDJ1= tcpSocketJ1->Receive(aDatosJ1, 1300);
			if (lDJ1 > 0)
			{
				if (lDJ1 < 1300)
					aDatosJ1[lDJ1] = '\0';
				ProcesarDatos(aDatosJ1, mapaEnemigos);
			}
			else if (lDJ1 == 0)
			{
				break;
			}
			char aDatosJ2[1300];
			int lDJ2 = tcpSocketJ2->Receive(aDatosJ2, 1300);
			if (lDJ2 > 0)
			{
				if (lDJ2 < 1300)
					aDatosJ2[lDJ2] = '\0';
				ProcesarDatos(aDatosJ2, mapaEnemigos);
			}
			else if (lDJ2 == 0)
			{
				break;
			}
			if (lDJ1 > 0 || lDJ2 > 0)
			{
				strMapaEnemigos = mapaEnemigos.GeneraMapaEnemigos();
				chMapa = strMapaEnemigos.c_str();
				tcpSocketJ1->Send(chMapa);
				tcpSocketJ2->Send(chMapa);
			}
		}

	}
}

int main(int argc, char ** argv)
{
	std::string funcion = argv[1];
	std::string direccion = argv[2];
	

	SocketTools::CargarLibreria();
	
	if (funcion == "servidor")
	{
		Servidor(direccion);
	}
	else if (funcion == "cliente")
	{
		//Cliente(direccion);
		Game game("The naive walking", 800, 600, direccion);

		try {
			game.run();
		}
		catch (std::exception e) {
			std::cerr << "ERROR: " << e.what() << std::endl;
		}

	}

	SocketTools::DescargarLibreria();
	return 0;
}