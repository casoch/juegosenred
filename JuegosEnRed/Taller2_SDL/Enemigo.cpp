#include "Enemigo.h"
#include <ctime>
#include <map>
#include "GameConstants.h"

Enemigo::Enemigo(int x, int y)
{
	this->x = x;
	this->y = y;
}

Enemigo::Enemigo(const Enemigo & enemigo):Enemigo(enemigo.x, enemigo.y)
{
	
}


bool Enemigo::operator==(Enemigo & enemigo)
{
	return enemigo.x == x && enemigo.y == y;
}

int Enemigo::GetX()
{
	return x;
}

int Enemigo::GetY()
{
	return y;
}

Enemigo::~Enemigo()
{
}
