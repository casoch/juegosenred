#include "Recepcion.h"
#include "SocketUtil.h"
#include "SocketAddressFactory.h"
#include <iostream>



Recepcion::Recepcion(std::string funcion, std::string IP, int16_t puerto, Buffer * _buffer, ControlSalida * _salida)
{
	this->funcion = funcion;
	buffer = _buffer;
	salida = _salida;
	char aPuerto[6];
	_itoa(puerto, aPuerto, 10);
	IP = IP + ":" + aPuerto;
	direccion = *(SocketAddressFactory::CreateIPv4FromString(IP));

	if (this->funcion == "servidor")
	{
		tcpDispatcher = SocketUtil::CreateTCPSocket();
		int err = tcpDispatcher->Bind(direccion);
		tcpDispatcher->Listen();
	}
	else if (this->funcion == "cliente")
	{
		tcpSocket = SocketUtil::CreateTCPSocket();
		int result = tcpSocket->Connect(direccion);
	}
}

void Recepcion::operator()()
{
	if (funcion == "servidor")
	{
		SocketAddress RemoteAddress;
		tcpSocket = tcpDispatcher->Accept(RemoteAddress);
	}
	while (!salida->EsSalida())
	{
		char cadena[1300];
		int longDatos = tcpSocket->Receive(cadena, 1300);

		if (longDatos > 0)
		{
			cadena[longDatos] = '\0';
			buffer->Encolar(cadena);
			int cmp = strcmp("exit", cadena);
			if (cmp == 0)
			{
				salida->Salir();
			}
			buffer->Mostrar();
		}
	}
}


Recepcion::~Recepcion()
{
	std::cout << "Destructor de recepción" << std::endl;
	//system("pause");
}


