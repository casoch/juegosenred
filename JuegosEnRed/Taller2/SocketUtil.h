#pragma once
#include "TCPSocket.h"


enum SocketAddressFamily { INET = AF_INET, INET6 = AF_INET6 };


class SocketUtil
{

public:

	static TCPSocketPtr CreateTCPSocket()
	{
		SOCKET s = socket(AF_INET, SOCK_STREAM, 0);
		if (s != INVALID_SOCKET)
		{
			TCPSocket* tcps = new TCPSocket(s);
			return TCPSocketPtr(tcps);
		}
		else
		{
			SocketUtil::ReportError("SocketUtil::CreateTCPSocket");
			return nullptr;
		}
	}


	static int CargarLibreria();

	static int DescargarLibreria();

	static void ReportError(std::string error);

	static int GetLastError();
};
