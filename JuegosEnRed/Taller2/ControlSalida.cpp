#include "ControlSalida.h"



ControlSalida::ControlSalida()
{
	exit = 0;
}

void ControlSalida::Salir()
{
	exit = 1;
}

bool ControlSalida::EsSalida()
{
	return exit == 1;
}


ControlSalida::~ControlSalida()
{
}
