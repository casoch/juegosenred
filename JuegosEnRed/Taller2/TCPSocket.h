#pragma once
#include "SocketAddress.h"




class TCPSocket
{
private:
	friend class SocketUtil;
	SOCKET mSocket;
	TCPSocket(SOCKET inSocket) :mSocket(inSocket) {}
public:
	int Connect(const SocketAddress& inAddress);
	int Bind(const SocketAddress& inToAddress);
	int Listen(int inBackLog = 32);
	std::shared_ptr<TCPSocket> Accept(SocketAddress& inFromAddress);
	int Send(const void* inData, int inLen);
	int Receive(void* inBuffer, int inLen);
	~TCPSocket();
};

typedef std::shared_ptr<TCPSocket> TCPSocketPtr;
