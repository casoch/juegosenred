#pragma once
#include "Envio.h"


class Recepcion
{
private:
	std::string funcion;
	TCPSocketPtr tcpDispatcher;
	TCPSocketPtr tcpSocket;
	SocketAddress direccion;
	Buffer* buffer;
	ControlSalida* salida;
	
public:
	Recepcion(std::string funcion, std::string IP, int16_t puerto, Buffer* _buffer, ControlSalida* _salida);
	void operator() ();
	~Recepcion();
};


