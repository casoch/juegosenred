#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
#include <memory>
#include <string>

class SocketAddress
{
private:
	//friend class UDPSocket;
	friend class TCPSocket;
	sockaddr mSockAddr;
	sockaddr_in* GetAsSockAddrIn()
	{
		return reinterpret_cast<sockaddr_in*> (&mSockAddr);
	}
public:
	SocketAddress() {}
	//Me dan la direcci�n IP entera y el puerto y me inicializa la direcci�n del socket
	SocketAddress(uint32_t inAddress, uint16_t inPort)
	{
		GetAsSockAddrIn()->sin_family = AF_INET;
		GetAsSockAddrIn()->sin_addr.S_un.S_addr = (inAddress);
		GetAsSockAddrIn()->sin_port = (inPort);

		sockaddr_in* saPtr = GetAsSockAddrIn();
		sockaddr_in sa = *saPtr;
		char a = 'u';
	}


	SocketAddress(PCWSTR inAddress, uint16_t inPort)
	{
		memset(GetAsSockAddrIn()->sin_zero, 0, sizeof(GetAsSockAddrIn()->sin_zero));
		GetAsSockAddrIn()->sin_family = AF_INET;

		InetPton(AF_INET, inAddress, &GetAsSockAddrIn()->sin_addr);
		GetAsSockAddrIn()->sin_port = (inPort);

		sockaddr_in* saPtr = GetAsSockAddrIn();
		sockaddr_in sa = *saPtr;
		char a = 'u';
	}

	//Para poder inicializar la direcci�n del socket desde un socket gen�rico
	SocketAddress(const sockaddr& inSockAddr)
	{
		memcpy(&mSockAddr, &inSockAddr, sizeof(sockaddr));
	}
	
	void puertoOK()
	{
		
		GetAsSockAddrIn()->sin_port = htons(GetAsSockAddrIn()->sin_port);
	}
	
	//size_t garantiza poder contener el tama�o m�s grande que pueda retornar GetSize()
	uint32_t GetSize() const 
	{
		return sizeof(sockaddr);
	}

	friend std::ostream& operator<<(std::ostream& os, SocketAddress& socketAddress)
	{
		sockaddr_in* sin = socketAddress.GetAsSockAddrIn();
		int b1 = sin->sin_addr.S_un.S_un_b.s_b1;
		int b2 = sin->sin_addr.S_un.S_un_b.s_b2;
		int b3 = sin->sin_addr.S_un.S_un_b.s_b3;
		int b4 = sin->sin_addr.S_un.S_un_b.s_b4;
		int port = sin->sin_port;
		
		os << b1 << "." << b2 << "." << b3 << "." << b4 << ":" << port;
		return os;
	}

	std::string toString()
	{
		sockaddr_in* sin = GetAsSockAddrIn();
		std::string retorno("");
		retorno += sin->sin_addr.S_un.S_un_b.s_b1;
		retorno += ".";
		retorno += sin->sin_addr.S_un.S_un_b.s_b2;
		retorno += ".";
		retorno += sin->sin_addr.S_un.S_un_b.s_b3;
		retorno += ".";
		retorno += sin->sin_addr.S_un.S_un_b.s_b4;
		retorno += ":";
		retorno += sin->sin_port;
		
			
		return retorno;
	}

};
typedef std::shared_ptr<SocketAddress> SocketAddressPtr;
