#pragma once
#include "Recepcion.h"
#include "SocketUtil.h"



int main(int argc, char** argv)
{
	SocketUtil::CargarLibreria();
	char funcion[10];
	strcpy_s(funcion, argv[1]);

	char IP[30];
	strcpy_s(IP, argv[2]);
	std::string strIP = IP;
	int16_t miPuertoRecibir = atoi(argv[3]);
	int16_t miPuertoEnviar = atoi(argv[4]);
	
	Buffer buffer;
	ControlSalida salida;
	Envio escritura(funcion, IP, miPuertoEnviar, &buffer, &salida);
	Recepcion lectura(funcion, IP, miPuertoRecibir, &buffer, &salida);
	
	std::thread tWrite(escritura);
	std::thread tRead(lectura);
	
	tRead.join();
	tWrite.join();

	
	

	
	//system("pause");
	SocketUtil::DescargarLibreria();
		
}


