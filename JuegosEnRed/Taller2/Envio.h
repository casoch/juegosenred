#pragma once
#include "TCPSocket.h"
#include "Buffer.h"
#include "ControlSalida.h"

class Envio
{
private:
	std::string funcion;
	TCPSocketPtr tcpDispatcher;
	TCPSocketPtr tcpSocket;
	SocketAddress direccion;
	Buffer* buffer;
	ControlSalida* salida;
public:
	Envio(std::string funcion, std::string IP, int16_t puerto, Buffer* _buffer, ControlSalida* _salida);
	void operator() ();
	void EnviarExit();
	~Envio();
};

