#include "SocketUtil.h"
#include <iostream>


int SocketUtil::CargarLibreria()
{
	WSADATA wsa;
	int result = WSAStartup(MAKEWORD(2, 2), &wsa);
	return result;
}

int SocketUtil::DescargarLibreria()
{
	int result = WSACleanup();
	return result;
}

void SocketUtil::ReportError(std::string error)
{
	std::cout << "Error en SocketUtil: " << error << std::endl;
}

int SocketUtil::GetLastError()
{
	return 0;
}
