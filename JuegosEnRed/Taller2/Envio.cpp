#include "Envio.h"
#include <iostream>
#include "StringUtils.h"
#include "SocketUtil.h"
#include "SocketAddressFactory.h"





Envio::Envio(std::string funcion, std::string IP, int16_t puerto, Buffer* _buffer, ControlSalida* _salida)
{
	this->funcion = funcion;
	buffer = _buffer;
	salida = _salida;
	char aPuerto[6];
	_itoa(puerto, aPuerto, 10);
	IP = IP + ":" + aPuerto;
	direccion = *(SocketAddressFactory::CreateIPv4FromString(IP));

	if (this->funcion == "servidor")
	{
		tcpDispatcher = SocketUtil::CreateTCPSocket();
		int err = tcpDispatcher->Bind(direccion);
		tcpDispatcher->Listen();
	}
	else if (this->funcion == "cliente")
	{
		tcpSocket = SocketUtil::CreateTCPSocket();
		int result = tcpSocket->Connect(direccion);
	}
}

void Envio::operator()()
{
	SocketAddress direccionRemota;
	if (funcion == "servidor")
	{
		tcpSocket = tcpDispatcher->Accept(direccionRemota);
	}
	do {
		char datos[1300];

		Buffer::gotoxy(WRITE_POS_X, WRITE_POS_Y);
		std::cout << "> ";
		int longitud = StringUtils::PideDatosTeclado(datos);
		//std::cout << "Longitud: " << longitud << std::endl;
		//std::cout << "Estoy enviado " << datos << std::endl;
		//std::cout << "Enviando por " << remotoPuerto << std::endl;
		
		int sendBytes = tcpSocket->Send(datos, longitud);
		if (sendBytes > 0)
		{
			//std::cout << "Se han enviado " << sendBytes << std::endl;
			char datosEncolar[1300];
			strcpy_s(datosEncolar, datos);
			std::string strEncolar = datosEncolar;
			buffer->Encolar(datosEncolar);

			int cmp = strcmp(datos, "exit");
			if (cmp == 0)
			{
				salida->Salir();
			}
			buffer->Mostrar();
		}
		else
		{
			std::cout << "Error al enviar " << sendBytes << std::endl;
		}

	} while (!(salida->EsSalida()));	
}

void Envio::EnviarExit()
{
	int sendBytes = tcpSocket->Send("exit", 4);
	if (sendBytes > 0)
	{
	}
}

Envio::~Envio()
{
	std::cout << "Destructor de envio" << std::endl;
	//system("pause");
}
