#pragma once
#include <string>

class Item
{
private:
	char nombre[20];
	int peso;
	int puntos;

public:
	Item(const char nombre[], int peso, int puntos);
	Item(const Item& item);
	int getPeso();
	int getPuntos();
	bool operator==(const Item& item);
	friend std::ostream& operator<<(std::ostream& os, Item& item);
	virtual ~Item();
};

