#include "Listado.h"
#include <exception>
#include <vector>

template<class T>
Listado<T>::Listado()
{
}

template<class T>
void Listado<T>::insertar(T objInsertar)
{
	aListado.push_back(objInsertar);
}

template<class T>
T Listado<T>::operator[](int posicion)
{
	int size = aListado.size();
	if (size <= posicion || size < 0)
	{
		throw std::exception("Posici�n no permitida");
	}
	return aListado[posicion];
}

template<class T>
void Listado<T>::eliminar(T objEliminar)
{
	for (size_t i = 0; i < aListado.size(); i++)
	{
		T objActual = aListado.pop_back();
		if (objActual != objEliminar)
		{
			aListado.push_back(objActual);
		}
	}
	
}

template<class T>
void Listado<T>::eliminar(int posEliminar)
{
	for (size_t i = 0; i < aListado.size(); i++)
	{
		T objActual = aListado.pop_back();
		if (i!=posEliminar)
		{
			aListado.push_back(objActual);
		}
	}
}

template<class T>
Listado<T>::~Listado()
{
}

template<class T>
std::ostream & operator<<(std::ostream & os, Listado<T>& item)
{
	int size = item.aListado.size();
	for (size_t i = 0; i < length; i++)
	{
		os << item.aListado[i];
	}
	return os;
}
