#pragma once
#include "Item.h"

class Casco : public Item
{
public:
	Casco(char nombre[20], int peso, int puntos);
	Casco(Casco& casco);
	~Casco();
};


