#pragma once
#include <string>
class Figura
{
private:
	float posX, posY, d1;
	std::string color;
protected:
	void metodo() {}
public:
	Figura(float posX, float posY, std::string color, float d1);
	Figura(Figura& figura);
	virtual void prueba();
	virtual ~Figura();
};

