#pragma once
#include "Figura.h"

class Rectangulo : public Figura
{
private:
	float altura;
public:
	Rectangulo(float posX, float posY, float base, float altura, std::string color);
	void prueba();
	~Rectangulo();
};

