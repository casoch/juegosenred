#include "Habilidad.h"
#include <cstring>
#include <iostream>


Habilidad::Habilidad(const char nombre[20], int puntosExperienciaMinimos)
{
	strcpy_s(this->nombre, nombre);
	this->puntosExperienciaMinimos = puntosExperienciaMinimos;
}

Habilidad::Habilidad(const Habilidad & habilidad):Habilidad(habilidad.nombre, habilidad.puntosExperienciaMinimos)
{
	std::cout << "Constructor de copia de habilidad" << std::endl;
}

bool Habilidad::operator==(Habilidad & item)
{
	int okNombre = strcmp(nombre, item.nombre);
	if (okNombre == 0 && item.puntosExperienciaMinimos == puntosExperienciaMinimos)
	{
		return true;
	}
	return false;
}

void Habilidad::incExperiencia()
{
	puntosExperienciaMinimos++;
}


Habilidad::~Habilidad()
{
	std::cout << "Destructor de habilidad" << std::endl;
	system("pause");
}

std::ostream & operator<<(std::ostream & os, Habilidad & habilidad)
{
	os << "Nombre: " << habilidad.nombre << "\n";
	os << "Nivel: " << habilidad.puntosExperienciaMinimos << "\n";
	return os;
}
