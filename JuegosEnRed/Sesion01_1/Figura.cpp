#include "Figura.h"
#include <iostream>

Figura::Figura(float posX, float posY, std::string color, float d1)
{
	this->posX = posX;
	this->posY = posY;
	this->color = color;
	this->d1 = d1;
}

Figura::Figura(Figura & figura) :Figura(figura.posX, figura.posY, figura.color, figura.d1)
{
}

void Figura::prueba()
{
	std::cout << "prueba de figura" << std::endl;
}

Figura::~Figura()
{
	std::cout << "Destructor de figura" << std::endl;
	system("pause");
}
