#pragma once
#include <vector>

template <class T>
class Listado
{
private:
	std::vector<T> aListado;
public:
	Listado() 
	{
	}

	Listado(Listado<T>& listado)
	{
		aListado = listado;
	}

	void insertar(const T& objInsertar)
	{
		aListado.push_back(objInsertar);
	}

	T operator[](int posicion)
	{
		int size = aListado.size();
		if (size <= posicion || size < 0)
		{
			throw std::exception("Posici�n no permitida");
		}
		return aListado[posicion];
	}

	void eliminar(const T& objEliminar)
	{
		std::vector<T> aNew;
		for (size_t i = 0; i < aListado.size(); i++)
		{
			T objActual = aListado[i];
			if (!(objActual == objEliminar))
			{
				aNew.push_back(objActual);
			}
		}
		aListado = aNew;
	}

	void eliminar(int posEliminar)
	{
		int size = aListado.size();
		if (size <= posEliminar || size < 0)
		{
			throw std::exception("Posici�n no permitida");
		}
		std::vector<T> aNew;
		for (size_t i = 0; i < aListado.size(); i++)
		{
			T objActual = aListado[i];
			if (i!=posEliminar)
			{
				aNew.push_back(objActual);
			}
		}
		aListado = aNew;
	}

	friend std::ostream& operator<<(std::ostream& os, Listado<T>& item)
	{
		int size = item.aListado.size();
		for (size_t i = 0; i < size; i++)
		{
			os << item.aListado[i];
		}
		return os;
	}

	~Listado()
	{
	}

};

