#pragma once
#include <vector>
#include "Arma.h"
#include "Armadura.h"
#include "Casco.h"
#include "Habilidad.h"
#include "Listado.h"
#include "ListadoItem.h"
typedef enum {
	WIZARD,
	WITCH_DOCTOR,
	DEMON_HUNTER,
	BARBARIAN,
	MONK
}TipoPersonaje;

typedef enum {
	CASCO,
	ARMADURA,
	ARMA
}TipoEquipacion;

class Personaje
{
private:
	 
	char nombre[20];
	TipoPersonaje tipo;
	int oro;
	int experiencia;
	Casco* casco;
	Armadura* armadura;
	Arma* arma;
	Item* equipacion[3];
	Listado<Habilidad> Habilidades;
	ListadoItem Inventario;

public:
	Personaje(const char* nombre, TipoPersonaje tipo);
	Personaje(const Personaje& personaje);
	Item* CambiarEquipacion(Item* item, TipoEquipacion tipoEquipacion);
	//Casco* CambiarCasco(Casco& casco);
	Armadura* CambiarArmadura(Armadura& armadura);
	Arma* CambiarArma(Arma& arma);
	void AnadirHabilidad(const Habilidad& habilidad);
	void AnadirItem(const Item& item);
	void DejarItem(const Item& item);
	void CojeOro(int cuantoOro);
	void SumaExperiencia();
	int Atacar(const Personaje& personajeAtacado);
	
	~Personaje();
};




