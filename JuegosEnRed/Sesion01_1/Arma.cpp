#include "Arma.h"
#include <iostream>


Arma::Arma(char nombre[20], int peso, int puntos):Item(nombre, peso, puntos)
{
}

Arma::Arma(Arma & armadura):Item(armadura)
{
	std::cout << "Constructor de copia" << std::endl;
}

Arma::~Arma()
{
	std::cout << "Destructor de arma" << std::endl;
	system("pause");
}
