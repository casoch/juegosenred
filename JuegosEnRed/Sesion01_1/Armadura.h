#pragma once
#include "Item.h"

class Armadura : public Item
{
public:
	Armadura(char nombre[20], int peso, int puntos);
	Armadura(Armadura& armadura);
	~Armadura();
};

