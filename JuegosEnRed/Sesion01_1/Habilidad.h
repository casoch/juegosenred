#pragma once
#include <iostream>

class Habilidad
{
private:
	char nombre[20];
	int puntosExperienciaMinimos;

public:
	Habilidad(const char nombre[20], int puntosExperienciaMinimos);
	Habilidad(const Habilidad& habilidad);
	bool operator==(Habilidad& item);
	void incExperiencia();
	friend std::ostream& operator<<(std::ostream& os, Habilidad& habilidad);
	~Habilidad();
};

