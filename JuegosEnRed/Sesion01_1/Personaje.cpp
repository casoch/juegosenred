#include "Personaje.h"
#include <cstring>
#include <iostream>



Personaje::Personaje(const char*  nombre, TipoPersonaje tipo)
{
	std::cout << "Constructor con parámetros de personaje" << std::endl;
	strcpy_s(this->nombre, nombre);
	this->tipo = tipo;
}

Personaje::Personaje(const Personaje & personaje)
{
	std::cout << "Constructor de copia de personaje" << std::endl;
	strcpy_s(nombre, personaje.nombre);
	tipo = personaje.tipo;
	oro = personaje.oro;
	experiencia = personaje.experiencia;
}


//Casco* Personaje::CambiarCasco(Casco * casco)
//{
//	Casco * retorno = this->casco;
//	this->casco = casco;
//	return retorno;
//}

Armadura* Personaje::CambiarArmadura(Armadura& armadura)
{
	Armadura* retorno = this->armadura;
	this->armadura = &armadura;
	return retorno;
}

Arma* Personaje::CambiarArma(Arma& arma)
{
	Arma* retorno = this->arma;
	this->arma = &arma;
	return retorno;
}

void Personaje::AnadirHabilidad(const Habilidad & habilidad)
{
	Habilidades.insertar(habilidad);
}

void Personaje::AnadirItem(const Item & item)
{
	//Inventario.insertar(item);
}

void Personaje::DejarItem(const Item & item)
{
	//Inventario.eliminar(item);
}

void Personaje::CojeOro(int cuantoOro)
{
	oro += cuantoOro;
}

void Personaje::SumaExperiencia()
{
	experiencia++;
}

int Personaje::Atacar(const Personaje&  personajeAtacado)
{
	int susPuntosDefensa = 0;
	if (personajeAtacado.casco != nullptr)
	{
		susPuntosDefensa += personajeAtacado.casco->getPuntos();
	}
	if (personajeAtacado.armadura != nullptr)
	{
		susPuntosDefensa += personajeAtacado.armadura->getPuntos();
	}
	int misPuntosAtaque = 0;
	if (arma != nullptr)
	{
		misPuntosAtaque += arma->getPuntos();
	}
	int dano = misPuntosAtaque - susPuntosDefensa;
	if (dano <= 0)
	{
		return 0;
	}
	else
	{
		return dano;
	}
}

Personaje::~Personaje()
{
	if (casco != nullptr)
		delete casco;
	if (armadura!= nullptr)
		delete armadura;
	if (arma!= nullptr)
		delete arma;
	std::cout << "Destructor de personaje." << std::endl;
}

