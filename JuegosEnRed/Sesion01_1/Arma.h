#pragma once
#include "Item.h"

class Arma : public Item
{
public:
	Arma(char nombre[20], int peso, int puntos);
	Arma(Arma& armadura);
	~Arma();
};

