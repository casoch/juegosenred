#include "Item.h"
#include <iostream>

Item::Item(const char nombre[], int peso, int puntos)
{
	strcpy_s(this->nombre, nombre);
	this->peso = peso;
	this->puntos = puntos;
}

Item::Item(const Item & item):Item(item.nombre, item.peso, item.puntos)
{
}

int Item::getPeso()
{
	return peso;
}

int Item::getPuntos()
{
	return puntos;
}

bool Item::operator==(const Item & item)
{
	int okNombre = strcmp(nombre, item.nombre);
	if (okNombre==0 && item.peso == peso &&item.puntos == puntos)
	{
		return true;
	}
	return false;
}

Item::~Item()
{
	std::cout << "Destructor de Item" << std::endl;
	system("pause");
}

std::ostream & operator<<(std::ostream & os, Item & item)
{
	os << "Nombre: " << item.nombre << "\n";
	os << "Peso: " << item.peso << "\n";
	os << "Puntos: " << item.puntos << "\n";
	return os;
}
