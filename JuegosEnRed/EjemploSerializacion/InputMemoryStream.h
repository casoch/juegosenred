#pragma once
#include <memory>
#include <vector>

class InputMemoryStream
{
private:
	char* mBuffer;
	uint32_t mHead;
	uint32_t mCapacity;
public:
	InputMemoryStream(char* _inBuffer, uint32_t _inByteCount) : mCapacity(_inByteCount), mHead(0)
	{
		mBuffer = _inBuffer;
	}

	~InputMemoryStream()
	{
		
	}

	uint32_t GetRemainingDataSize() const
	{
		return mCapacity - mHead;
	}

	std::string ReadString()
	{
		int length;
		Read(&length);
		char* buffer = new char[length+1];
		Read(buffer, length);
		buffer[length] = '\0';
		std::string str = std::string(buffer);
		
		delete[] buffer;
		return str;
	}

	void Read(void* _outData, uint32_t _inByteCount)
	{
		uint32_t resultHead = mHead + _inByteCount;
		if (resultHead > mCapacity)
		{
			throw std::exception("No data to read");
		}

		std::memcpy(_outData, mBuffer + mHead, _inByteCount);

		mHead = resultHead;
	}

	template<typename T> void Read(T* _outData)
	{
		static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value, "Este Read solo soporta tipos basicos.");
		Read(_outData, sizeof(*_outData));
	}

	template<typename T> void Read(std::vector<T>* _outVector)
	{
		size_t elementCount;
		Read(&elementCount);
		for (size_t i = 0; i < elementCount; i++)
		{
			T element;
			Read(&element);
			_outVector->push_back(element);
		}
	}
};


