#pragma once
#include <memory>
#include <vector>
#include <algorithm>

class OutputMemoryStream
{
private:
	char* mBuffer;
	uint32_t mHead;
	uint32_t mCapacity;
	void ReallocBuffer(uint32_t _newLength)
	{
		mBuffer = static_cast<char*>(std::realloc(mBuffer, _newLength));
		//TODO: Controlar error en realloc
		mCapacity = _newLength;
	}
public:
	OutputMemoryStream() :mBuffer(0), mHead(0), mCapacity(0)
	{
		ReallocBuffer(32);
	}
	~OutputMemoryStream()
	{
		std::free(mBuffer);
	};

	char* GetBufferPtr() const
	{
		return mBuffer;
	}

	void GetBuffer(char* _buffer) const
	{
		for (size_t i = 0; i < mHead; i++)
		{
			_buffer[i] = mBuffer[i];
		}
	}

	uint32_t GetLength() const
	{
		return mHead;
	}

	void Write(const void* _inData, size_t _inByteCount)
	{
		//Nos aseguramos de que hay espacio suficiente en mBuffer para copiar estos datos
		uint32_t resultHead = mHead + static_cast<uint32_t>(_inByteCount);
		if (resultHead > mCapacity)
		{
			ReallocBuffer(std::max(mCapacity * 2, resultHead));
		}

		//Copiar en el buffer a partir de head
		std::memcpy(mBuffer + mHead, _inData, _inByteCount);

		//Incrementamos el head para el siguiente write
		mHead = resultHead;
	}

	void WriteString(std::string _inString)
	{
		Write(_inString.size());
		Write(_inString.c_str(), _inString.size());
	}

	template<typename T> void Write(T _data)
	{
		static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value, "Este Write solo soporta tipos basicos.");
		Write(&_data, sizeof(_data));
	}

	template<typename T> void Write(const std::vector<T>& _inVector)
	{
		std::cout << "Se serializa el vector " << std::endl;
		size_t elementCount = _inVector.size();
		Write(elementCount);
		for (const T& element: _inVector)
		{
			Write(element);
		}
	}
	
	
};

