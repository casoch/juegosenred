#include "UDPSocket.h"
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include <Windows.h>
#include "SocketTools.h"

UDPSocket::UDPSocket():GenericSocket(SOCK_DGRAM)
{
}

int UDPSocket::SendTo(const void * data, int lenData, SocketAddress & to)
{
	sockaddr sa_to = to.GetAddress();
	int err = sendto(sock, (char*)data, lenData, 0, &sa_to, sizeof(sockaddr));
	if (err == -1)
	{
		SocketTools::ReportError("Error en UDPSocket::SendTo");
	}
	return err;
}

int UDPSocket::ReceiveFrom(void * data, int lenData, SocketAddress & from)
{
	sockaddr sa_from;
	int fromLen = sizeof(sockaddr);
	int bytesReceived = recvfrom(sock, (char*)data, lenData, 0, &sa_from, &fromLen);
	if (bytesReceived == -1)
	{
		SocketTools::ReportError("Error en UDPSocket::ReceiveFrom");
	}
	memcpy(&from.address, &sa_from, fromLen);
	return bytesReceived;
}

UDPSocket::~UDPSocket()
{

}
