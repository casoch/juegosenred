#pragma once
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include <Windows.h>
#include <memory>
#include <iostream>


class SocketAddress
{
private:
	sockaddr_in address;
	friend class UDPSocket;
public:
	SocketAddress();
	SocketAddress(uint8_t byte1, uint8_t byte2, uint8_t byte3, uint8_t byte8, uint16_t puerto);
	SocketAddress(const SocketAddress& socketAddress);
	friend std::ostream& operator<<(std::ostream& os, SocketAddress& socketAddress);
	bool operator==(SocketAddress& sa_compare);
	sockaddr GetAddress();
	int SetAddress(const std::string& inString);
	~SocketAddress();
};