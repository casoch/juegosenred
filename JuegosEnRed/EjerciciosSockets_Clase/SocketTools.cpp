#include "SocketTools.h"
#include <iostream>
#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>
#include <Windows.h>


int SocketTools::CargarLibreria()
{
	WSADATA wsa;
	int result = WSAStartup(MAKEWORD(2, 2), &wsa);
	return result;
}

int SocketTools::DescargarLibreria()
{
	int result = WSACleanup();
	return result;
}

void SocketTools::ReportError(std::string msgError)
{
	int numError = WSAGetLastError();
	std::cout << "Error n�mero "<<numError<<" en " << msgError << std::endl;
}
