#include "GenericSocket.h"
#include "SocketTools.h"
#include <iostream>

GenericSocket::GenericSocket(int type)
{
	sock = socket(AF_INET, type, 0);
	if (sock == INVALID_SOCKET)
	{
		SocketTools::ReportError("Error al inicializar Socket");
	}
}

int GenericSocket::Bind(SocketAddress & address)
{
	sockaddr sa = address.GetAddress();
	int err = bind(sock, &sa, sizeof(sockaddr));
	if (err == -1)
	{
		SocketTools::ReportError("Error en GenericSocket::Bind");
	}
	return err;
}


GenericSocket::~GenericSocket()
{
	int err2 = closesocket(sock);
	if (err2 != NO_ERROR)
	{
		SocketTools::ReportError("Error en closesocket");
	}
	std::cout << "Paso por el destructor" << std::endl;
	
	
}
