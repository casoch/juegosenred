#pragma once
#include <vector>
#include "SocketAddress.h"

class ClientList :
	public std::vector<SocketAddress>
{
public:
	ClientList() {}
	int indexOf(SocketAddress& sa)
	{
		for (size_t i = 0; i < size(); i++)
		{
			SocketAddress saAux = this->at(i);
			if (saAux == sa)
			{
				return i;
			}
		}
		
		return -1;
	}
	~ClientList() {}
};

