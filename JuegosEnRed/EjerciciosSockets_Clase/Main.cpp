#pragma once
#include <vector>
#include "UDPSocket.h"
#include "SocketTools.h"
#include "ClientList.h"

void SoyServidor(std::string miDireccion)
{
	SocketAddress sa;
	sa.SetAddress(miDireccion);
	UDPSocket udpSocket;
	int err = udpSocket.Bind(sa);

	if (err == 0)
	{
		ClientList aClientes;

		while (true)
		{
			char data[1300];
			SocketAddress from;
			int bytesReceived = udpSocket.ReceiveFrom(data, 1300, from);
			if (bytesReceived > 0)
			{
				data[bytesReceived] = '\0';
				std::cout << ">> " << data << std::endl;
			}
			int posFrom = aClientes.indexOf(from);
			if (posFrom == -1)
			{
				aClientes.push_back(from);
				posFrom = aClientes.indexOf(from);
			}
			if (strcmp(data, "exit") == 0)
			{
				aClientes.erase(aClientes.begin()+ posFrom);
			}
			if (aClientes.size() == 0)
			{
				break;
			}
		}
	}
}

void SoyCliente(std::string direccionDestino)
{
	SocketAddress sa;
	sa.SetAddress(direccionDestino);
	UDPSocket udpSocket;
	while (true)
	{
		std::cout << ">>";
		std::string mensajeEnviar;
		std::getline(std::cin, mensajeEnviar);
		
		const char* strMensajeEnviar = mensajeEnviar.c_str();
		int errSend = udpSocket.SendTo(strMensajeEnviar, mensajeEnviar.size(), sa);
		if (errSend == 0)
		{
			std::cout << "Enviado." << std::endl;
		}
		if (mensajeEnviar == "exit")
		{
			break;
		}
	}
}

int main(int argc, char** argv)
{
	SocketTools::CargarLibreria();
	char funcion[10];
	strcpy_s(funcion, argv[1]);
	int cmp = strcmp(funcion, "servidor");
	if (cmp == 0)
	{
		std::string miDireccion = argv[2];
		SoyServidor(miDireccion);
	}
	cmp = strcmp(funcion, "cliente");
	if (cmp == 0)
	{
		std::string direccionDestino = argv[2];
		SoyCliente(direccionDestino);
	}
	
	SocketTools::DescargarLibreria();
}