#pragma once
#include <string>

class SocketTools
{
public:

	static int CargarLibreria();

	static int DescargarLibreria();

	static void ReportError(std::string msgError);

};

